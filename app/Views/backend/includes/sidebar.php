<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<div class="wrapper"><!-- Wrapper div has been closed at footer file. -->
  <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
<!--            <li class="nav-item d-none d-sm-inline-block">
                <a href="index3.html" class="nav-link">Home</a>
            </li>-->
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="button">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">0</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">0 Notifications</span>
                    <div class="dropdown-divider"></div>
<!--                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>-->
                </div>
            </li>
            <li class="nav-item dropdown user user-menu open">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="true">
                    <img src="<?= getBaseUrl() ?>assets/backend/images/user.png" class="user-image" alt="User Image">
                    <span class="hidden-xs"><?= ucfirst( $arrmixAdminSessionDetails['username'] ) ?></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="<?= getBaseUrl() ?>assets/backend/images/user.png" class="img-circle" alt="User Image">
                        <p><?= $arrmixAdminSessionDetails['first_name'] . ' ' . $arrmixAdminSessionDetails['last_name'] ?></p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="float-left">
                            <a href="javascript:void(0)" class="btn btn-info btn-flat"><i class="far fa-user-circle"></i> Profile</a>
                        </div>
                        <div class="float-right">
                            <a href="<?= getBaseUrl() ?>admin/authentication/logout" class="btn btn-danger btn-flat"><i class="fas fa-sign-out-alt"></i> Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
<!--            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                    <i class="fas fa-th-large"></i>
                </a>
            </li>-->
        </ul>
    </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?= getBaseUrl() ?>" class="brand-link">
            <img src="<?= getBaseUrl() ?>assets/backend/images/user.png" alt="Admin Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light"><?= getProjectName() ?></span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="<?= getBaseUrl() ?>assets/backend/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block"><?= $arrmixAdminSessionDetails['first_name'] . ' ' . $arrmixAdminSessionDetails['last_name'] ?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="<?= getBaseUrl() ?>admin/dashboard" class="nav-link <?= ( true == isset( $arrmixUriSegmentData[1] ) && 'dashboard' == $arrmixUriSegmentData[1] ) ? 'active' : ''?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= getBaseUrl() ?>admin/students" class="nav-link <?= ( true == isset( $arrmixUriSegmentData[1] ) && 'students' == $arrmixUriSegmentData[1] ) ? 'active' : ''?>">
                            <i class="nav-icon fas fa-users"></i>
                            <p>Students</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= getBaseUrl() ?>admin/uploading-videos" class="nav-link <?= ( true == isset( $arrmixUriSegmentData[1] ) && 'uploading-videos' == $arrmixUriSegmentData[1] ) ? 'active' : ''?>">
                            <i class="nav-icon fas fa-video"></i>
                            <p>Uploading Videos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= getBaseUrl() ?>admin/subjects" class="nav-link <?= ( true == isset( $arrmixUriSegmentData[1] ) && 'subjects' == $arrmixUriSegmentData[1] ) ? 'active' : ''?>">
                            <i class="nav-icon fas fa-book"></i>
                            <p>Subjects</p>
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="<?= getBaseUrl() ?>admin/standards" class="nav-link <?= ( true == isset( $arrmixUriSegmentData[1] ) && 'standards' == $arrmixUriSegmentData[1] ) ? 'active' : ''?>">
                            <i class="nav-icon fas fa-address-card"></i>
                            <p>Standards</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper custom-content-wrapper"><!-- Content Wrapper div has been closed at footer file. -->