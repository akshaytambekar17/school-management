<?php
namespace App\Controllers;

use App\Controllers\BaseController;

class BackendController extends BaseController {
    
    public $arrmixAdminSessionDetails;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
	parent::initController($request, $response, $logger);
        
        if( $this->session->get( 'admin_session' ) ) {
            $this->arrmixAdminSessionDetails = $this->session->get( 'admin_session' );
        }
        
    }            
    
    public function backendLayout( $arrmixData ) {
        $arrmixData['arrmixFlashData'] = $this->session->getFlashData();
        
        if( true == isset( $arrmixData['boolIsHeaderFooter'] ) ) {
            includesBackendHeaderFooter( $arrmixData );
        } else {
            $arrmixData['arrmixAdminSessionDetails'] = $this->arrmixAdminSessionDetails;
            $arrmixData['arrmixUriSegmentData'] = $this->request->uri->getSegments();
            includesBackendAll( $arrmixData );
        }
    }
    
    public function showAlertMessage( $arrmixFlashData ) {
        
        if( true == isArrVal( $arrmixFlashData ) ) {
            $arrmixData = [];
            
            if( true == isset( $arrmixFlashData['success'] ) ) {
                $arrmixData['strSuccessMessage'] = $arrmixFlashData['success'];
            } else if( true == isset( $arrmixFlashData['error'] ) ) {
                $arrmixData['strErrorMessage'] = $arrmixFlashData['error'];
            } else if( true == isset( $arrmixFlashData['info'] ) ) {
                $arrmixData['strInfoMessage'] =  $arrmixFlashData['info'];
            } else if( true == isset( $arrmixFlashData['warning'] ) ) {
                $arrmixData['strWarningMessage'] =  $arrmixFlashData['warning'];
            } 
            
            echo view( 'backend/includes/alert', $arrmixData );
        }
    }
    
    public function showBreadCrumbs( $arrmixBreadCrumbData ) {
        echo view( 'backend/includes/breadcrumb', $arrmixBreadCrumbData );
    }
    

    public static function createService() {
        return new \App\Controllers\BackendController();
    }
    
   
}
