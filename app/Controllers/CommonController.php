<?php
namespace App\Controllers;

use App\Controllers\BaseController;

class CommonController extends BaseController {
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
	parent::initController($request, $response, $logger);
    }            
    
    public function fetchCityListByStateId() {
        
        $arrmixRequestData = $this->request->getPost();
        $arrmixCityList = \App\Models\CitiesModel::createService()->where( 'state_id', $arrmixRequestData['state_id'] )->findAll();
        $intHiddenCityId = ( true == isIdVal( $arrmixRequestData['hidden_city_id'] ) ) ? $arrmixRequestData['hidden_city_id'] : '';
        
        $strHtml = [];
        if( true == isArrVal( $arrmixCityList ) ) {
            foreach( $arrmixCityList as $arrmixCityDetails ) {
                $strSelected = '';
                if( $intHiddenCityId == $arrmixCityDetails['city_id']) {
                    $strSelected = 'selected="selected"';
                }
                
                $strHtml[] = ' <option value="' . $arrmixCityDetails['city_id'] . '" ' . set_select( 'city_id', $arrmixCityDetails['city_id'] ) . ' ' . $strSelected . ' > ' . $arrmixCityDetails['city_name'] . '</option>';
            }
        }
        
        $this->response( $strHtml );
    }
    
    
    /**
     *  Static Methods  
     **/
    
    public static function createService() {
        return new \App\Controllers\BackendController();
    }
    
    
   
}
