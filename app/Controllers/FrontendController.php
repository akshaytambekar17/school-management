<?php
namespace App\Controllers;

use App\Controllers\BaseController;

class FrontendController extends BaseController {
    
    public $arrmixUserSessionDetails;
    public static $s_arrmixValidationErrors;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
	parent::initController($request, $response, $logger);
        
        if( $this->session->get( 'user_session' ) ) {
            $this->arrmixUserSessionDetails = $this->session->get( 'user_session' );
        }
        
    }            
    
    public function frontendLayout( $arrmixData ) {
        $arrmixData['arrmixFlashData'] = $this->session->getFlashData();
        
        if( true == isset( $arrmixData['boolIsHeaderFooter'] ) ) {
            includesFrontendHeaderFooter( $arrmixData );
        } else {
            $arrmixData['arrmixUserSessionDetails'] = $this->arrmixUserSessionDetails;
            $arrmixData['arrmixUriSegmentData'] = $this->request->uri->getSegments();
            includesFrontendAll( $arrmixData );
        }
    }
    
    public function showAlertMessage( $arrmixFlashData ) {
        
        if( true == isArrVal( $arrmixFlashData ) ) {
            $arrmixData = [];
            
            if( true == isset( $arrmixFlashData['success'] ) ) {
                $arrmixData['strSuccessMessage'] = $arrmixFlashData['success'];
            } else if( true == isset( $arrmixFlashData['error'] ) ) {
                $arrmixData['strErrorMessage'] = $arrmixFlashData['error'];
            } else if( true == isset( $arrmixFlashData['info'] ) ) {
                $arrmixData['strInfoMessage'] =  $arrmixFlashData['info'];
            } else if( true == isset( $arrmixFlashData['warning'] ) ) {
                $arrmixData['strWarningMessage'] =  $arrmixFlashData['warning'];
            } 
            
            echo view( 'frontend/includes/alert', $arrmixData );
        }
    }
    
    public function showBreadCrumbs( $arrmixBreadCrumbData ) {
        echo view( 'frontend/includes/breadcrumb', $arrmixBreadCrumbData );
    }
    
    public static function createService() {
        return new \App\Controllers\FrontendController();
    }
   
}
