<?php
namespace App\Controllers\Services;

use App\Controllers\ServicesController;
use App\Models\SubjectsModel;

class SubjectsController extends ServicesController {
    
    public function actionfetchSubjectList() {
        
        $arrmixSubjectList = SubjectsModel::createService()->findAll();
        
        $arrmixResponseData['success'] = false;
        $arrmixResponseData['message'] = 'No records found for the Subjects';
        
        if( true == isArrVal( $arrmixSubjectList ) ) {
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['message'] = 'Successfully fetch the subject list.';
            $arrmixResponseData['subject_list'] = $arrmixSubjectList;
        }
        
        return $arrmixResponseData;
    }
    
    public static function createService() {
        return new SubjectsController();
    }
    
}
