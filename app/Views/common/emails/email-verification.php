<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]--><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge" /><!--<![endif]-->
        <meta name="viewport" content="width=device-width" />
    </head>
    <body>
        <table>
            <tbody>
                <tr>
                    <td><p> Hi <?= $first_name . ' ' . $last_name ?>, </p></td>
                </tr>
                <tr>
                    <td>
                        <p>Thank you for registration with us. Please verify you email address to get access our system.</p>
                        <p>This verification link will valid only for 1 hour</p>
                        <br>
                            <a href="<?= getBaseUrl() ?>authentications/email-verification-link?token=<?= urlencode( $token ) ?>" style="text-decoration: none;color: #fff;background-color: #224ac6;border-color: #224ac6;font-size: 1.2rem;border-radius: 10rem;padding: 0.75rem 1rem;">
                                Verify Email Now
                            </a>
                        <br>
                        <br>
                        <p>Assuring you of our best service, always!</p>    
                        <p>Thank you,</p>
                        <p>Team <?= getCompanyName() ?>.</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
