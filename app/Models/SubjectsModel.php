<?php namespace App\Models;

use App\Models\CommonModel;

class SubjectsModel extends CommonModel
{
    protected $table      = 'tbl_subjects';
    protected $primaryKey = 'subject_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'subject_name', 'created_by', 'updated_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
   
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public static function createService() {
        return new SubjectsModel();
    }
    
    public function createBuilderObject() {
        return self::createService()->builder();
    }
    
    public function findSujectList() {
        $objQuery = $this->createBuilderObject()->select( 'tbl_subjects.*, tstd.standard_name' )
                                                ->join( 'tbl_standards tstd', 'tbl_subjects.standard_id = tstd.standard_id' )
                                                ->orderBy( 'tbl_subjects.subject_id', 'desc' )
                                                ->get();
        return $objQuery->getResultArray();
    }
    
    public function add( $arrmixInsertData ) {
        
        $arrmixInsertData['updated_at'] = CURRENT_DATETIME;
        $arrmixInsertData['created_by'] = $this->getCreatedBy();
        $arrmixInsertData['updated_by'] = $this->getUpdatedBy();
        
        $objQuery = $this->createBuilderObject()->insert( $arrmixInsertData );
        if( $objQuery ) {
            return $objQuery->connID->insert_id;
        } 
        
        return false;
    }
    
    public function addBatch( $arrmixInsertList ) {
        
        $objQuery = $this->createBuilderObject()->insertBatch( $arrmixInsertList );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
    public function edit( $arrmixUpdateData ) {
        
        $arrmixUpdateData['updated_by'] = $this->getUpdatedBy();
        $arrmixUpdateData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->where( 'subject_id', $arrmixUpdateData['subject_id'] )
                                                ->update( $arrmixUpdateData );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
}