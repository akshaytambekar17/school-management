<?php namespace App\Models;

use App\Models\CommonModel;

class UploadingVideosModel extends CommonModel
{
    protected $table      = 'tbl_uploading_videos';
    protected $primaryKey = 'uploading_video_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'standard_id', 'subject_id', 'topic', 'videos', 'created_by', 'updated_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
   
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public static function createService() {
        return new UploadingVideosModel();
    }
    
    public function createBuilderObject() {
        return self::createService()->builder();
    }
    
    public function findUploadingVideoList() {
        $objQuery = $this->createBuilderObject()->select( 'tbl_uploading_videos.*, tstd.standard_name, tsub.subject_name' )
                                                ->join( 'tbl_subjects tsub', 'tbl_uploading_videos.subject_id = tsub.subject_id' )
                                                ->join( 'tbl_standards tstd', 'tbl_uploading_videos.standard_id = tstd.standard_id' )
                                                ->orderBy( 'tbl_uploading_videos.uploading_video_id', 'desc' )
                                                ->get();
        return $objQuery->getResultArray();
    }
    
    public function findUploadingVideoDetailsByUploadingVideoId( $intUploadingVideoId ) {
        $objQuery = $this->createBuilderObject()->select( 'tbl_uploading_videos.*, tstd.standard_name, tsub.subject_name' )
                                                ->join( 'tbl_subjects tsub', 'tbl_uploading_videos.subject_id = tsub.subject_id' )
                                                ->join( 'tbl_standards tstd', 'tbl_uploading_videos.standard_id = tstd.standard_id' )
                                                ->where( 'tbl_uploading_videos.uploading_video_id', $intUploadingVideoId )
                                                ->get();
        return $objQuery->getRowArray();
    }
    
    public function findUploadingVideoListBySubjectId( $intSubjectId ) {
        
        $objQuery = $this->createBuilderObject()->select( 'tbl_uploading_videos.*, tstd.standard_name, tsub.subject_name' )
                                                ->join( 'tbl_subjects tsub', 'tbl_uploading_videos.subject_id = tsub.subject_id' )
                                                ->join( 'tbl_standards tstd', 'tbl_uploading_videos.standard_id = tstd.standard_id' )
                                                ->where( 'tbl_uploading_videos.subject_id', $intSubjectId )
                                                ->orderBy( 'tbl_uploading_videos.uploading_video_id', 'desc' )
                                                ->get();
        return $objQuery->getResultArray();
        
    }
    
    public function findUploadingVideoListByStandardId( $intStandardId ) {
        
        $objQuery = $this->createBuilderObject()->select( 'tbl_uploading_videos.*, tstd.standard_name, tsub.subject_name' )
                                                ->join( 'tbl_subjects tsub', 'tbl_uploading_videos.subject_id = tsub.subject_id' )
                                                ->join( 'tbl_standards tstd', 'tbl_uploading_videos.standard_id = tstd.standard_id' )
                                                ->where( 'tbl_uploading_videos.standard_id', $intStandardId )
                                                ->orderBy( 'tbl_uploading_videos.uploading_video_id', 'desc' )
                                                ->get();
        return $objQuery->getResultArray();
        
    }
    
    public function findUploadingVideoListByStandardIdBySubjectId( $intStandardId, $intSubjectId ) {
        
        $objQuery = $this->createBuilderObject()->select( 'tbl_uploading_videos.*, tstd.standard_name, tsub.subject_name' )
                                                ->join( 'tbl_subjects tsub', 'tbl_uploading_videos.subject_id = tsub.subject_id' )
                                                ->join( 'tbl_standards tstd', 'tbl_uploading_videos.standard_id = tstd.standard_id' )
                                                ->where( 'tbl_uploading_videos.standard_id', $intStandardId )
                                                ->where( 'tbl_uploading_videos.subject_id', $intSubjectId )
                                                ->orderBy( 'tbl_uploading_videos.uploading_video_id', 'desc' )
                                                ->get();
        return $objQuery->getResultArray();
    }
    
    public function add( $arrmixInsertData ) {
        
        $arrmixInsertData['created_by'] = $this->getCreatedBy();
        $arrmixInsertData['updated_by'] = $this->getUpdatedBy();
        
        $arrmixInsertData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->insert( $arrmixInsertData );
        if( $objQuery ) {
            return $objQuery->connID->insert_id;
        } 
        
        return false;
    }
    
    public function addBatch( $arrmixInsertList ) {
        
        $objQuery = $this->createBuilderObject()->insertBatch( $arrmixInsertList );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
    public function edit( $arrmixUpdateData ) {
        
        $arrmixUpdateData['updated_by'] = $this->getUpdatedBy();
        $arrmixUpdateData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->where( 'uploading_video_id', $arrmixUpdateData['uploading_video_id'] )
                                                ->update( $arrmixUpdateData );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
}