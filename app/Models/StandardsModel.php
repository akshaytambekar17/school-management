<?php namespace App\Models;

use App\Models\CommonModel;

class StandardsModel extends CommonModel
{
    protected $table      = 'tbl_standards';
    protected $primaryKey = 'standard_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'standard_name', 'created_by', 'updated_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
   
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public static function createService() {
        return new StandardsModel();
    }
    
    public function createBuilderObject() {
        return self::createService()->builder();
    }
    
    public function add( $arrmixInsertData ) {
        
        $arrmixInsertData['updated_at'] = CURRENT_DATETIME;
        $arrmixInsertData['created_by'] = $this->getCreatedBy();
        $arrmixInsertData['updated_by'] = $this->getUpdatedBy();
        
        $objQuery = $this->createBuilderObject()->insert( $arrmixInsertData );
        if( $objQuery ) {
            return $objQuery->connID->insert_id;
        } 
        
        return false;
    }
    
    public function addBatch( $arrmixInsertList ) {
        
        $objQuery = $this->createBuilderObject()->insertBatch( $arrmixInsertList );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
    public function edit( $arrmixUpdateData ) {
        
        $arrmixUpdateData['updated_by'] = $this->getUpdatedBy();
        $arrmixUpdateData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->where( 'standard_id', $arrmixUpdateData['standard_id'] )
                                                ->update( $arrmixUpdateData );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
}