<?php
namespace App\Controllers\Backend;

use App\Controllers\BackendController;
use App\Models\StandardsModel;

class StandardsController extends BackendController {
    
    public function actionList() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixStandardList = StandardsModel::createService()->findAll();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Standards';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Standards', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Standards';
        $arrmixData['view'] = 'standards/list';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixStandardList'] = $arrmixStandardList;
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionAdd() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Standards';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Standards', 'is_active' => false, 'href' => getBaseUrl() . 'admin/standards' ],
            ['name' => 'Add Standards', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Add Standard';
        $arrmixData['strCardTitle'] = 'Add Standard';
        $arrmixData['view'] = 'standards/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost();
            
            if( $this->validation->run( $arrmixRequestData , 'validateStandard' ) ) {
                $arrmixInsertData = $arrmixRequestData;
                
                $intStandardId = StandardsModel::createService()->add( $arrmixInsertData );
                
                if( true == isIdVal( $intStandardId ) ) {
                    $this->session->setFlashdata( 'success', 'New standard ' . $arrmixInsertData['standard_name'] . ' has been added successfully.' );
                    return redirect()->route( 'admin/standards' );
                } else {
                    $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                }
                
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionEdit() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getGet();
        
        if( false == isset( $arrmixRequestData['standard_id'] ) || false == isIdVal( $arrmixRequestData['standard_id'] ) ) {
            $this->session->setFlashdata( 'error', 'Invalid StandardId.' );
            return redirect()->route( 'admin/standards' );
        }
        
        $arrmixStandardDetails = StandardsModel::createService()->find( $arrmixRequestData['standard_id'] );
        
        if( false == isArrVal( $arrmixStandardDetails ) ) {
            $this->session->setFlashdata( 'error', 'Data not found for given StandardId : ' . $arrmixRequestData['standard_id'] );
            return redirect()->route( 'admin/standards' );
        }
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Standards';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Standards', 'is_active' => false, 'href' => getBaseUrl() . 'admin/standards' ],
            ['name' => $arrmixStandardDetails['standard_name'], 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Edit Standard';
        $arrmixData['strCardTitle'] = 'Edit Standard';
        $arrmixData['view'] = 'standards/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixStandardDetails'] = $arrmixStandardDetails;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost(); 
            
            if( $this->validation->run( $arrmixRequestData , 'validateStandard' ) ) {
                
                $arrmixUpdateData = $arrmixRequestData;
                
                $boolResult = StandardsModel::createService()->edit( $arrmixUpdateData );
                
                if( true ==$boolResult ) {
                    $this->session->setFlashdata( 'success', 'Standard ' . $arrmixRequestData['standard_name'] . ' has been updated successfully.' );
                    return redirect()->route( 'admin/standards' );
                } else {
                    $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                }
                
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionDelete() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getPost();
        
        if( false == isset( $arrmixRequestData['standard_id'] ) || false == isIdVal( $arrmixRequestData['standard_id'] ) ) {
            $arrmixResponseData['success'] = false;
            $arrmixResponseData['message'] = 'Invalid StandardId';
            
            $this->response( $arrmixResponseData );
        }
        
        $arrmixResponseData['success'] = false;
        $arrmixResponseData['message'] = 'Something went wrong. Please try later';
        
        $objResponse = StandardsModel::createService()->delete( $arrmixRequestData['standard_id'] );
        
        if( $objResponse && true == $objResponse->connID->affected_rows ) {
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['message'] = 'Successfully Deleted';
        } 
        
        $this->response( $arrmixResponseData );
    } 
    
}
