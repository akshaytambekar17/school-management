<?php namespace App\Models;

use App\Models\CommonModel;

class StudentsModel extends CommonModel
{
    protected $table      = 'tbl_students';
    protected $primaryKey = 'student_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'user_id', 'standard_id', 'roll_number', 'created_by', 'updated_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
   
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public static function createService() {
        return new StudentsModel();
    }
    
    public function createBuilderObject() {
        return self::createService()->builder();
    }
    
    public function findStudentList() {
        $objQuery = $this->createBuilderObject()->select( 'tbl_students.*, tu.first_name, tu.last_name, tu.username, tu.mobile_no, tu.gender, tstd.standard_name' )
                                    ->join( 'tbl_standards tstd', 'tstd.standard_id = tbl_students.standard_id' )
                                    ->join( 'tbl_users tu', 'tu.user_id = tbl_students.user_id' )
                                    ->orderBy( 'tbl_students.student_id', 'desc' )
                                    ->get();
        return $objQuery->getResultArray();
    }
    
    public function findStudentDetailsByStudentId( $intStudentId ) {
        $objQuery = $this->createBuilderObject()->select( 'tbl_students.*, tu.first_name, tu.last_name, tu.username, tu.mobile_no, tu.gender, tu.email_id, tu.address, tu.state_id, tu.city_id' )
                                    ->join( 'tbl_users tu', 'tu.user_id = tbl_students.user_id' )
                                    ->where( 'tbl_students.student_id', $intStudentId )
                                    ->get();
        return $objQuery->getRowArray();
    }
    
    public function add( $arrmixInsertData ) {
        
        $arrmixInsertData['created_by'] = $this->getCreatedBy();
        $arrmixInsertData['updated_by'] = $this->getUpdatedBy();
        $arrmixInsertData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->insert( $arrmixInsertData );
        if( $objQuery ) {
            return $objQuery->connID->insert_id;
        } 
        
        return false;
    }
    
    public function addBatch( $arrmixInsertList ) {
        
        $objQuery = $this->createBuilderObject()->insertBatch( $arrmixInsertList );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
    public function edit( $arrmixUpdateData ) {
        
        $arrmixUpdateData['updated_by'] = $this->getUpdatedBy();
        $arrmixUpdateData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->where( 'student_id', $arrmixUpdateData['student_id'] )
                                                ->update( $arrmixUpdateData );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
    
    
    
}