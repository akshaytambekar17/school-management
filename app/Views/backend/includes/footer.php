<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
            </div> <!-- ./End content-wrapper --- added at ending in sidebar file-->
            <?php if( false == isset( $boolIsHideFooter ) ) { ?>
                <footer class="main-footer">
                    <strong>Copyright &copy; 2020 <a href="http://www.diligencetech.com/">Diligence Tech</a>.</strong>
                    All rights reserved.
                    <div class="float-right d-none d-sm-inline-block">
                            <b>Version</b> 1.0
                    </div>
                </footer>
            <?php } ?>      
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
          <!-- /.control-sidebar -->
        </div>
        <div class="modal fade" id="js-modal-delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="delete-model">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="text-center popup-content">  
                            <br>
                            <h5> By clicking on <span>"YES"</span>, <span id="js-modal-body-header-name"></span> will be deleted permanently. Do you wish to proceed?</h5><br><br>
                            <input  type="hidden" id="js-modal-body-hidden-id"> 
                            <button type="button" class="btn btn-success modal-box-button" id="js-modal-body-confirm-button" >Yes</button>
                            <button type="button" class="btn btn-danger modal-box-button" data-dismiss="modal" >No</button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <!-- ./End wrapper --- added at starting of sidebar -->

        <!-- jQuery -->
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
<!--        <script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
         Resolve conflict in jQuery UI tooltip with Bootstrap tooltip 
        <script>
          $.widget.bridge('uibutton', $.ui.button)
        </script>-->
        <!-- Bootstrap 4 -->
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- ChartJS -->
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/chart.js/Chart.min.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/moment/moment.min.js"></script>
        <!-- daterangepicker -->
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Select2 -->
        <script src="<?= getBaseUrl() ?>assets/backend/plugins/select2/js/select2.full.min.js"></script>
        
        <!-- AdminLTE App -->
        <script src="<?= getBaseUrl() ?>assets/backend/dist/js/adminlte.js"></script>
        
        <script src="<?= getBaseUrl() ?>assets/backend/js/backend.js"></script>
        <!-- Common JS -->
        <script src="<?= getBaseUrl() ?>assets/common/js/common.js"></script>
        
        <script>
            function getBaseUrl() {
                var strBaseUrl = "<?php echo getBaseUrl()?>";
                return strBaseUrl;
            }
        </script>
    </body>
</html>


