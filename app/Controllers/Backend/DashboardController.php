<?php
namespace App\Controllers\Backend;

use App\Controllers\BackendController;

class DashboardController extends BackendController {
    
    public function actionDashboard() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Dashboard';
        
        $arrmixDashboardData = [
            'student_count' => \App\Models\StudentsModel::createService()->countAllResults(),
            'subject_count' => \App\Models\SubjectsModel::createService()->countAllResults(),
            'standard_count' => \App\Models\StandardsModel::createService()->countAllResults(),
            'uploading_video_count' => \App\Models\UploadingVideosModel::createService()->countAllResults()
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Dashboard';
        $arrmixData['view'] = 'dashboard/admin';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixDashboardData'] = $arrmixDashboardData;
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionForgotPassword() {
        printDie( 'In the Forgot Password' );
    }
    
    public function actionLogout() {
        printDie( 'In the Logout' );
    }
    
    public function checkIsLogin() {
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        return true;
    }
   
}
