<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <a href="<?= getBaseUrl() ?>admin/uploading-videos/add" class="btn btn-outline-success"> <i class="fas fa-plus-square"></i>&nbsp;&nbsp;Upload Video</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped js-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Standard</th>
                            <th>Subject</th>
                            <th>Topic</th>
                            <th>Download</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if( true == isArrVal( $arrmixUploadingVideoList ) ) {
                                foreach( $arrmixUploadingVideoList as $arrmixUploadingVideoDetails ) {
                        ?>
                                <tr>
                                    <td><?= $arrmixUploadingVideoDetails['uploading_video_id']?></td>
                                    <td><?= $arrmixUploadingVideoDetails['standard_name']?></td>
                                    <td><?= $arrmixUploadingVideoDetails['subject_name']?></td>
                                    <td><?= $arrmixUploadingVideoDetails['topic']?></td>
                                    <td><a href="<?= getBaseUrl() ?>uploaded-videos/<?= $arrmixUploadingVideoDetails['videos']?>" download><?= $arrmixUploadingVideoDetails['original_video_name']?></a></td>
                                    <td class="project-actions">
                                        <a class="btn btn-outline-info btn-sm" href="<?= getBaseUrl() ?>admin/uploading-videos/edit?uploading_video_id=<?= $arrmixUploadingVideoDetails['uploading_video_id']?>">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm" href="javascript:void(0)" id="js-delete-uploading-video" data-uploading_video_id="<?= $arrmixUploadingVideoDetails['uploading_video_id']?>" data-uploading_video_name="<?= $arrmixUploadingVideoDetails['standard_name'] . '-' . $arrmixUploadingVideoDetails['subject_name'] ?>" onclick="showDeleteModal(this)">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                        <?php } } else { ?>    
                                <tr>
                                    <td colspan="7" class="text-center">No videos record found</td>
                                </tr>
                        <?php } ?>    
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        
        $( '#js-modal-body-confirm-button' ).on( 'click', function() {
            var intUploadingVideoId = $( "#js-modal-body-hidden-id" ).val();
            
            $.ajax({
                type: "POST",
                url: "<?php echo getBaseUrl(); ?>" + "admin/uploading-videos/delete",
                dataType: 'json',
                data: { 'uploading_video_id' : intUploadingVideoId },
                success: function( arrmixResponseData ) {
                    $( '#js-modal-delete-confirmation' ).modal( 'hide' );
                    
                    resetDeleteConfirmationModal();
                    if( true == arrmixResponseData.success ) {
                        showAlertMessage( 'success', arrmixResponseData.message )
                        setTimeout(function(){ 
                            location.reload();
                        }, 2000);
                    }else{
                        showAlertMessage( 'danger', 'Something went wrong. Please try later' );
                    }
                }
            });
        });
    });
    
    function showDeleteModal(ths){
        var intUploadingVideoId = $( ths ).data( 'uploading_video_id' );
        var strHeaderName = 'Video of <b>' + $( ths ).data( 'uploading_video_name' ) + '</b>';
        showDeleteConfirmationModal( strHeaderName, intUploadingVideoId );
    }
</script>
