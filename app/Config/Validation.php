<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
        
        public $validateLogin = [
            'username'  => 'required|alpha_numeric',
            'password'  => 'required|min_length[6]',
        ];
        
        public $validateStandard = [
            'standard_name' => ['label' => 'Standard Name', 'rules' => 'required|alpha_numeric_punct|max_length[25]']
        ];
        
        public $validateSubject = [
            'subject_name' => ['label' => 'Subject Name', 'rules' => 'required|alpha_numeric_space|max_length[25]'],
            'standard_id'  => ['label' => 'Standard', 'rules' => 'required|is_natural'],
        ];
        
        public $validateStudent = [
            'first_name'       => ['label' => 'First Name', 'rules' => 'required|alpha_space|max_length[10]'],
            'last_name'        => ['label' => 'Last Name', 'rules' => 'required|alpha_space|max_length[10]'],
            'mobile_no'        => ['label' => 'Mobile Number', 'rules' => 'required|is_natural|exact_length[10]'],
            'gender'           => ['label' => 'Gender', 'rules' => 'required|is_natural'],
            'roll_number'      => ['label' => 'Roll Number', 'rules' => 'required|is_natural'],
            'standard_id'      => ['label' => 'Standard', 'rules' => 'required|is_natural'],
            'state_id'         => ['label' => 'State', 'rules' => 'required|is_natural'],
            'city_id'          => ['label' => 'City', 'rules' => 'required|is_natural'],
            'address'          => ['label' => 'Address', 'rules' => 'required|alpha_numeric_punct|max_length[100]'],
            'username'         => ['label' => 'Username', 'rules' => 'required|alpha_numeric|is_unique[tbl_users.username,user_id,{user_id}]'],
            'password'         => ['label' => 'Password', 'rules' => 'trim|required|min_length[6]'],
            'confirm_password' => ['label' => 'Confirm Password', 'rules' => 'trim|required|matches[password]|min_length[6]'],
        ];
        
        public $validateUpdateStudent = [
            'first_name'       => ['label' => 'First Name', 'rules' => 'required|alpha_space|max_length[10]'],
            'last_name'        => ['label' => 'Last Name', 'rules' => 'required|alpha_space|max_length[10]'],
            'mobile_no'        => ['label' => 'Mobile Number', 'rules' => 'required|is_natural|exact_length[10]'],
            'gender'           => ['label' => 'Gender', 'rules' => 'required|is_natural'],
            'roll_number'      => ['label' => 'Roll Number', 'rules' => 'required|is_natural'],
            'standard_id'      => ['label' => 'Standard', 'rules' => 'required|is_natural'],
            'state_id'         => ['label' => 'State', 'rules' => 'required|is_natural'],
            'city_id'          => ['label' => 'City', 'rules' => 'required|is_natural'],
            'address'          => ['label' => 'Address', 'rules' => 'required|alpha_numeric_punct|max_length[100]'],
        ];
        
        public $validateUploadingVideo = [
            'standard_id' => ['label' => 'Standard', 'rules' => 'required|is_natural'],
            'subject_id'  => ['label' => 'Subject', 'rules' => 'required|is_natural'],
            'topic'       => ['label' => 'Topic', 'rules' => 'required|alpha_numeric_punct'],
        ];
        
        
        /**
	* Services Validation
	*/        
        public $validateServiceLogin = [
            'username'    => 'required|alpha_numeric',
            'password'    => 'required|min_length[6]',
            'standard_id' => 'required|is_natural',
        ];
        
}
