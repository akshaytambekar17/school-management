<?php
namespace App\Controllers\Backend;

use App\Controllers\BackendController;
use App\Models\SubjectsModel;

class SubjectsController extends BackendController {
    
    public function actionList() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixSubjectList = SubjectsModel::createService()->findSujectList();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Subjects';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Subjects', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Subjects';
        $arrmixData['view'] = 'subjects/list';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixSubjectList'] = $arrmixSubjectList;
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionAdd() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixStandardList = \App\Models\StandardsModel::createService()->findAll();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Subjects';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Subjects', 'is_active' => false, 'href' => getBaseUrl() . 'admin/subjects' ],
            ['name' => 'Add Subjects', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Add Subject';
        $arrmixData['strCardTitle'] = 'Add Subject';
        $arrmixData['view'] = 'subjects/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixStandardList'] = $arrmixStandardList;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost();
            
            if( $this->validation->run( $arrmixRequestData , 'validateSubject' ) ) {
                $arrmixInsertData = $arrmixRequestData;
                
                $intSubjectId = SubjectsModel::createService()->add( $arrmixInsertData );
                
                if( true == isIdVal( $intSubjectId ) ) {
                    $this->session->setFlashdata( 'success', 'New subject <b>' . $arrmixInsertData['subject_name'] . '</b> has been added successfully.' );
                    return redirect()->route( 'admin/subjects' );
                } else {
                    $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                }
                
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionEdit() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getGet();
        
        if( false == isset( $arrmixRequestData['subject_id'] ) || false == isIdVal( $arrmixRequestData['subject_id'] ) ) {
            $this->session->setFlashdata( 'error', 'Invalid SubjectId.' );
            return redirect()->route( 'admin/subjects' );
        }
        
        $arrmixSubjectDetails = SubjectsModel::createService()->find( $arrmixRequestData['subject_id'] );
        
        if( false == isArrVal( $arrmixSubjectDetails ) ) {
            $this->session->setFlashdata( 'error', 'Data not found for given SubjectId : ' . $arrmixRequestData['subject_id'] );
            return redirect()->route( 'admin/subjects' );
        }
        
        $arrmixStandardList = \App\Models\StandardsModel::createService()->findAll();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Subjects';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Subjects', 'is_active' => false, 'href' => getBaseUrl() . 'admin/subjects' ],
            ['name' => $arrmixSubjectDetails['subject_name'], 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Edit Subject';
        $arrmixData['strCardTitle'] = 'Edit Subject';
        $arrmixData['view'] = 'subjects/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixSubjectDetails'] = $arrmixSubjectDetails;
        $arrmixData['arrmixStandardList'] = $arrmixStandardList;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost(); 
            
            if( $this->validation->run( $arrmixRequestData , 'validateSubject' ) ) {
                
                $arrmixUpdateData = $arrmixRequestData;
                
                $boolResult = SubjectsModel::createService()->edit( $arrmixUpdateData );
                
                if( true ==$boolResult ) {
                    $this->session->setFlashdata( 'success', 'Subject <b>' . $arrmixRequestData['subject_name'] . '</b> has been updated successfully.' );
                    return redirect()->route( 'admin/subjects' );
                } else {
                    $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                }
                
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionDelete() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getPost();
        
        if( false == isset( $arrmixRequestData['subject_id'] ) || false == isIdVal( $arrmixRequestData['subject_id'] ) ) {
            $arrmixResponseData['success'] = false;
            $arrmixResponseData['message'] = 'Invalid SubjectId';
            
            $this->response( $arrmixResponseData );
        }
        
        $arrmixResponseData['success'] = false;
        $arrmixResponseData['message'] = 'Something went wrong. Please try later';
        
        $objResponse = SubjectsModel::createService()->delete( $arrmixRequestData['subject_id'] );
        
        if( $objResponse && true == $objResponse->connID->affected_rows ) {
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['message'] = 'Successfully Deleted';
        } 
        
        $this->response( $arrmixResponseData );
    } 
    
}
