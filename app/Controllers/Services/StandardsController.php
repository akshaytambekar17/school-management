<?php
namespace App\Controllers\Services;

use App\Controllers\ServicesController;
use App\Models\StandardsModel;

class StandardsController extends ServicesController {
    
    public function actionfetchStandardList() {
        
        $arrmixStandardList = StandardsModel::createService()->findAll();
        
        $arrmixResponseData['success'] = false;
        $arrmixResponseData['message'] = 'No records found for the Standards';
        
        if( true == isArrVal( $arrmixStandardList ) ) {
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['message'] = 'Successfully fetch the standard list.';
            $arrmixResponseData['subject_list'] = $arrmixStandardList;
        }
        
        return $arrmixResponseData;
    }
    

    public static function createService() {
        return new StandardsController();
    }    
}
