<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><?= $strCardTitle; ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="<?= getBaseUrl() ?>admin/standards/<?= true == isset( $arrmixStandardDetails ) ? 'edit?standard_id=' . $arrmixStandardDetails['standard_id'] : 'add' ?>"  method="post" name="form">
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label label-required" for="js-standard-name">Standard Name</label>
                        <input type="text" class="form-control" id="js-standard-name" name="standard_name" placeholder="Standard Name" value="<?= ( true == isset( $arrmixStandardDetails ) ) ? $arrmixStandardDetails['standard_name'] : set_value( 'standard_name' ) ?>">
                        <?= getValidationError( 'standard_name' ) ?>
                    </div>
                </div>
                <?php if( true == isset( $arrmixStandardDetails ) ) { ?>
                        <input type="hidden" name="standard_id" value="<?= $arrmixStandardDetails['standard_id']?>">
                <?php } ?>    
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                    <a href="<?= getBaseUrl() ?>admin/standards" class="btn btn-outline-danger"> Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>

