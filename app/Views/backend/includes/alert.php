<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php if( true == isset( $strSuccessMessage ) && isVal( $strSuccessMessage ) ) { ?>
    <div class="alert alert-success alert-dismissible js-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="icon fas fa-check"></i> <?= $strSuccessMessage ?>
    </div>
<?php } else if( true == isset( $strErrorMessage ) && isVal( $strErrorMessage ) ) { ?>
    <div class="alert alert-danger alert-dismissible js-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="icon fas fa-ban"></i> <?= $strErrorMessage ?>
    </div>
<?php } else if( true == isset( $strInfoMessage ) && isVal( $strInfoMessage ) ) { ?>
    <div class="alert alert-info alert-dismissible js-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="icon fas fa-info"></i> <?= $strInfoMessage ?>
    </div>
<?php } else if( true == isset( $strWarningMessage ) && isVal( $strWarningMessage ) ) { ?>
    <div class="alert alert-warning alert-dismissible js-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="icon fas fa-exclamation-triangle"></i> <?= $strWarningMessage ?>
    </div>
<?php } ?>

