<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers\Backend');
$routes->setDefaultController('AuthenticationsController');
$routes->setDefaultMethod('actionLogin');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'AuthenticationsController::actionLogin');
//$routes->get('/admin', '\App\Controllers\Backend\AuthenticationsController::index');
//$routes->match( ['get', 'post'],'admin/authentication/login', '\App\Controllers\Backend\AuthenticationsController::actionLogin' );
//$routes->match( ['get', 'post', 'add'],'admin/authentication/forgot-password', '\App\Controllers\Backend\AuthenticationsController::actionForgotPassword' );

$routes->add( 'admin/authentication/login', 'AuthenticationsController::actionLogin' );
$routes->add( 'admin/authentication/logout', 'AuthenticationsController::actionLogout' );

$routes->add( 'admin/dashboard', 'DashboardController::actionDashboard' );

$routes->add( 'admin/standards', 'StandardsController::actionList' );
$routes->add( 'admin/standards/add', 'StandardsController::actionAdd' );
$routes->add( 'admin/standards/edit', 'StandardsController::actionEdit' );
$routes->add( 'admin/standards/delete', 'StandardsController::actionDelete' );

$routes->add( 'admin/subjects', 'SubjectsController::actionList' );
$routes->add( 'admin/subjects/add', 'SubjectsController::actionAdd' );
$routes->add( 'admin/subjects/edit', 'SubjectsController::actionEdit' );
$routes->add( 'admin/subjects/delete', 'SubjectsController::actionDelete' );

$routes->add( 'admin/students', 'StudentsController::actionList' );
$routes->add( 'admin/students/add', 'StudentsController::actionAdd' );
$routes->add( 'admin/students/edit', 'StudentsController::actionEdit' );
$routes->add( 'admin/students/delete', 'StudentsController::actionDelete' );

$routes->add( 'admin/uploading-videos', 'UploadingVideosController::actionList' );
$routes->add( 'admin/uploading-videos/add', 'UploadingVideosController::actionAdd' );
$routes->add( 'admin/uploading-videos/edit', 'UploadingVideosController::actionEdit' );
$routes->add( 'admin/uploading-videos/delete', 'UploadingVideosController::actionDelete' );

$routes->addRedirect( 'admin', 'admin/authentication/login' );

$routes->add( 'fetch-city-list-by-state-id', '\App\Controllers\CommonController::fetchCityListByStateId' );


/**
 * Web Services
 */

$routes->post( 'service/school-management/api-portal', '\App\Controllers\Services\ApiPortalController::index' );
$routes->post( 'service/school-management/login', '\App\Controllers\Services\ApiPortalController::actionLogin' );


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
