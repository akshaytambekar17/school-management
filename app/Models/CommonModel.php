<?php 
namespace App\Models;

use CodeIgniter\Model;

class CommonModel extends Model
{
    public function getCreatedBy() {
        
        if( true == isset( $this->arrmixAdminSessionDetails ) && true == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            $arrmixData['created_by'] = $this->arrmixAdminSessionDetails['user_id'];
        } else if( true == isset( $this->arrmixUserSessionDetails ) && true == isArrVal( $this->arrmixUserSessionDetails ) ) {
            $arrmixData['created_by'] = $this->arrmixUserSessionDetails['user_id'];
        } else {
            $arrmixData['created_by'] = ADMIN_ID;
        }
        
        return $arrmixData;
    }
    
    public function getUpdatedBy() {
        
        if( true == isset( $this->arrmixAdminSessionDetails ) && true == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            $arrmixData['updated_by'] = $this->arrmixAdminSessionDetails['user_id'];
        } else if( true == isset( $this->arrmixAdminSessionDetails ) && true == isArrVal( $this->arrmixUserSessionDetails ) ) {
            $arrmixData['updated_by'] = $this->arrmixUserSessionDetails['user_id'];
        } else {
            $arrmixData['updated_by'] = ADMIN_ID;
        }
        
        return $arrmixData;
    }
    
}