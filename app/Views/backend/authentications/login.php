<script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:void(0)"><b><?= getProjectName() ?></b></a>
    </div>
    <div class="js-show-alert-message">
        <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="<?= getBaseUrl() ?>admin/authentication/login"  method="post">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Username" name="username" value="<?= set_value('username') ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    <?= getValidationError( 'username' ) ?>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="<?= set_value('password') ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    <?= getValidationError( 'password' ) ?>
                </div>
                <div class="row mb-3">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-block btn-outline-primary">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

<!--            <p class="mb-1">
                <a href="<?= getBaseUrl() ?>admin/forgot-password">I forgot my password</a>
            </p>  -->
        </div>
        <!-- /.login-card-body -->
    </div>
</div>