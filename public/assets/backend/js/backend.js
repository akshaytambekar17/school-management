/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$( function () {
    $( ".js-datatable" ).DataTable({
        "responsive": true,
        "autoWidth": false,
        "aaSorting": [[0, "desc"]]
    });
    
    $( '.js-picker-date' ).datepicker({
        format : 'dd/mm/yyyy',
        autoclose: true,
        endDate: new Date()
    });
} );

//function resetDeleteConfirmationModal() {
//    $( "#js-modal-body-hidden-id" ).val(0);
//    $( "#js-modal-body-header-name" ).html( '' );
//}
//
//function showDeleteConfirmationModal( strHeaderName, intHiddenId ) {
//    $( "#js-modal-body-header-name" ).html( strHeaderName );
//    $( "#js-modal-body-hidden-id" ).val( intHiddenId );
//    $( "#js-modal-delete-confirmation" ).modal( 'show' );
//}

function getDistrictByState( intStateId ){
    var intHiddenDistrictId = $( '#js-hidden-district-id' ).val();
    
    $.ajax({
        type: "POST",
        url:  getBaseUrl() + 'fetch-district-list-by-state-id',
        data: { 'state_id' : intStateId, 'hidden_district_id' : intHiddenDistrictId },
        dataType: "html",
        success: function( objResult ){
            var strHtml = $.parseJSON( objResult );
            $( '#js-district-id' ).html('<option disabled selected> Select District</option>');
            $( '#js-district-id' ).append( strHtml );
        }
    });
}

function getDistributorList(){
    $.ajax({
        type: "POST",
        url:  getBaseUrl() + 'fetch-distributor-list',
        data: { 'user_type_id' : 3 },
        dataType: "html",
        success: function( objResult ){
            var strHtml = $.parseJSON( objResult );
            $( '#js-distributor-id' ).html('<option disabled selected> Select Distributor</option>');
            $( '#js-distributor-id' ).append( strHtml );
        }
    });
}

function getUserSiteListByUserId( intUserId, boolUploadTimeData = '' ) {
    var intHiddenUserSiteId = $( '#js-hidden-user-site-id' ).val();
    $.ajax({
        type: "POST",
        url:  getBaseUrl() + 'fetch-user-site-list-by-user-id',
        data: { 'user_id' : intUserId, 'hidden_user_site_id' : intHiddenUserSiteId, 'upload_time_data': boolUploadTimeData },
        dataType: "html",
        success: function( objResult ){
            var strHtml = $.parseJSON( objResult );
            $( '#js-user-site-id' ).html('<option disabled selected> Select User Site</option>');
            $( '#js-user-site-id' ).append( strHtml );
        }
    });
}
