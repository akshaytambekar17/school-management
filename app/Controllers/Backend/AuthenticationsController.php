<?php
namespace App\Controllers\Backend;

use App\Controllers\BackendController;

class AuthenticationsController extends BackendController {

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }            
    
    public function actionLogin() {
        
        if( true == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/dashboard' );
        } 
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Login';
        $arrmixData['boolIsHideFooter'] = true;
        $arrmixData['boolIsHeaderFooter'] = true;
        $arrmixData['boolIsLogin'] = true;
        $arrmixData['view'] = 'authentications/login';
        
        if( $this->request->getPost() ) {
            $arrmixPostData = $this->request->getPost();
            if( $this->validation->run( $arrmixPostData , 'validateLogin' ) ) {
                
                $arrmixUserDetails = \App\Models\UsersModel::createService()->where( 'username', $arrmixPostData['username'] )->findOne();
                
                if( true == isArrVal( $arrmixUserDetails ) ) {
                    if( USER_TYPE_ADMIN == $arrmixUserDetails['user_type_id'] ) {
                        if( $arrmixPostData['password'] == $this->decryptPassword( $arrmixUserDetails['password'] ) ) {
                            $this->session->set( 'admin_session', $arrmixUserDetails );
                            $this->session->setFlashdata( 'success', 'Successfully Login' );

                            return redirect()->route( 'admin/dashboard' );
                        } else {
                            $this->session->setFlashdata( 'error', 'Invalid Password' );
                        }
                    } else {
                        $this->session->setFlashdata( 'error', 'Invalid User' );
                    }    
                } else {
                    $this->session->setFlashdata( 'error', 'Invalid Username.' );
                }
            } else {
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
            
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionForgotPassword() {
        printDie( 'In the Forgot Password' );
    }
    
    public function actionLogout() {
        $this->session->destroy();
        return redirect( 'admin/authentication/login', 'refresh' ); 
    }

   
}
