<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <a href="<?= getBaseUrl() ?>admin/subjects/add" class="btn btn-outline-success"> <i class="fas fa-plus-square"></i>&nbsp;&nbsp;Add Subject</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped js-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Standard Name</th>
                            <th>Subject Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if( true == isArrVal( $arrmixSubjectList ) ) {
                                foreach( $arrmixSubjectList as $arrmixSubjectDetails ) {
                        ?>
                                <tr>
                                    <td><?= $arrmixSubjectDetails['subject_id']?></td>
                                    <td><?= $arrmixSubjectDetails['standard_name']?></td>
                                    <td><?= $arrmixSubjectDetails['subject_name']?></td>
                                    <td class="project-actions">
                                        <a class="btn btn-outline-info btn-sm" href="<?= getBaseUrl() ?>admin/subjects/edit?subject_id=<?= $arrmixSubjectDetails['subject_id']?>">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm" href="javascript:void(0)" id="js-delete-subject" data-subject_id="<?= $arrmixSubjectDetails['subject_id']?>" data-subject_name="<?= $arrmixSubjectDetails['subject_name']?>" onclick="showDeleteModal(this)">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                        <?php } } else { ?>    
                                <tr>
                                    <td colspan="7" class="text-center">No Subjects found</td>
                                </tr>
                        <?php } ?>    
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        
        $( '#js-modal-body-confirm-button' ).on( 'click', function() {
            var intSubjectId = $( "#js-modal-body-hidden-id" ).val();
            
            $.ajax({
                type: "POST",
                url: "<?php echo getBaseUrl(); ?>" + "admin/subjects/delete",
                dataType: 'json',
                data: { 'subject_id' : intSubjectId },
                success: function( arrmixResponseData ) {
                    $( '#js-modal-delete-confirmation' ).modal( 'hide' );
                    
                    resetDeleteConfirmationModal();
                    if( true == arrmixResponseData.success ) {
                        var strAlertMessage = 'Subject ' + $( '#js-modal-body-header-name' ).text() + ' has been deleted successfully';
                        showAlertMessage( 'success', strAlertMessage )
                        setTimeout(function(){ 
                            location.reload();
                        }, 2000);
                    }else{
                        showAlertMessage( 'danger', 'Something went wrong. Please try later' );
                    }
                }
            });
        });
    });
    
    function showDeleteModal(ths){
        var intSubjectId = $( ths ).data( 'subject_id' );
        var strHeaderName = 'Subject <b>' + $( ths ).data( 'subject_name' ) + '</b>';
        showDeleteConfirmationModal( strHeaderName, intSubjectId );
    }
</script>
