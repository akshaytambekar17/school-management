<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function prints($data) {
    echo "<pre>";
    print_r($data);
}

function printDie($data) {
    echo "<pre>";
    print_r($data);
    die;
}

function getBaseUrl() {
    return getenv( 'app.baseURL' );
}

function includesBackendHeaderFooter( $arrmixData  ) {
    echo view( 'backend/includes/header', $arrmixData );
    echo view( 'backend/' . $arrmixData['view'], $arrmixData );
    echo view( 'backend/includes/footer', $arrmixData );
}

function includesBackendAll( $arrmixData  ) {
    echo view( 'backend/includes/header', $arrmixData );
    echo view( 'backend/includes/sidebar', $arrmixData );
    echo view( 'backend/' . $arrmixData['view'], $arrmixData );
    echo view( 'backend/includes/footer', $arrmixData );
}

function includesFrontendHeaderFooter( $arrmixData  ) {
    echo view( 'frontend/includes/header', $arrmixData );
    echo view( 'frontend/' . $arrmixData['view'], $arrmixData );
    echo view( 'frontend/includes/footer', $arrmixData );
}

function includesFrontendAll( $arrmixData  ) {    
    echo view( 'frontend/includes/header', $arrmixData );
    echo view( 'frontend/includes/sidebar', $arrmixData );
    echo view( 'frontend/' . $arrmixData['view'], $arrmixData );
    echo view( 'frontend/includes/footer', $arrmixData );
}

function isArrVal( $arrmixData ) {
    if( !empty( $arrmixData ) && is_array( $arrmixData ) && count( $arrmixData ) > 0 ) {
        return true;
    } else {
        return false;
    }
}

function isStrVal( $strData ) {
    if ( !empty( $strData ) && is_string( $strData ) ) {
        return true;
    } else {
        return false;
    }
}

function isIdVal( $intData ) {
    if ( !empty( $intData ) && is_numeric( $intData ) ) {
        return true;
    } else {
        return false;
    }
}

function isVal( $var ) {
    if ( !empty( $var ) ) {
        return true;
    } else {
        return false;
    }
}

function getValidationError( $strField ) {
    $arrmixValidationErrors = App\Controllers\BaseController::getFormValidationErrors();
    
    if( true == isArrVal( $arrmixValidationErrors ) && true == isset( $arrmixValidationErrors[$strField] ) ) {
        $strHtml = '<span class="error invalid-feedback js-validation-error">' . $arrmixValidationErrors[$strField] . '</span>';
        return $strHtml;
    } 

    return;
}

function printLastSql() {
    $objInstance = & get_instance();
    printDie( $objInstance->db->last_query() );
}

function printFormValidationError() {
    printDie( App\Controllers\BackendController::getFormValidationErrors() );
}


function convertDateFormatToStandardFormat( $strDate ) {
    $strDate = str_replace( '/', '-', $strDate );
    return date( 'Y-m-d', strtotime( $strDate ) );
}

function replaceDateTimeDashFormatToUnderscore( $strDateTime ) {
    $strReplaceDateTime = preg_replace( '/\b-\b/', '_', $strDateTime );
    $strReplaceDateTime = preg_replace( '/\b:\b/', '_', $strReplaceDateTime );
    $strReplaceDateTime = preg_replace( '/\b \b/', '_', $strReplaceDateTime );
    
    return $strReplaceDateTime;
}

function rekey_array( $arrmixData, $strColumnName ) {
    return array_column( $arrmixData, NULL, $strColumnName );
}

function getProjectName() {
    return PROJECT_NAME;
}


