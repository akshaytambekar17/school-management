/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function createBarChart( arrstrLabel, arrDataValue, strBarColor ) {
    $( '#js-bar-chart-canvas-section' ).html('');
    $( '#js-bar-chart-canvas-section' ).html( '<canvas id="js-bar-chart" style="height:230px"></canvas>' );
    var areaChartData = {
        labels  : arrstrLabel,
        datasets: [{
            label               : 'Pumps',
            fillColor           : 'rgba(210, 214, 222, 1)',
            strokeColor         : 'rgba(210, 214, 222, 1)',
            pointColor          : 'rgba(210, 214, 222, 1)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : arrDataValue
          }]
    }
    
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $( '#js-bar-chart' ).get(0).getContext('2d')
    var objBarChart                      = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    barChartData.datasets[0].fillColor   = strBarColor
    barChartData.datasets[0].strokeColor = strBarColor
    barChartData.datasets[0].pointColor  = strBarColor
    var barChartOptions                  = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero        : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : true,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - If there is a stroke on each bar
        barShowStroke           : true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth          : 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing         : 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing       : 1,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to make the chart responsive
        responsive              : true,
        maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    objBarChart.Bar(barChartData, barChartOptions)
    
}

function createLineGraph( arrstrLabel, arrDataValue, strLineColor ) {
    console.log( arrstrLabel );
    console.log( arrDataValue );
    $( '#js-line-chart-canvas-section' ).html('');
    $( '#js-line-chart-canvas-section' ).html( '<canvas id="js-line-chart" style="height:250px"></canvas>' );
    var arrLineChartData = {
        labels  : arrstrLabel,
        datasets: [
            {
                label               : 'Digital Goods',
                fillColor           : 'rgba(60,141,188,0.9)',
                strokeColor         : strLineColor,
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : arrDataValue
            }
        ]
    }

      var arrLineChartOptions = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : true,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : true,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
    }

    //-------------
    //- LINE CHART -
    //--------------
    var objLineChartCanvas       = $('#js-line-chart').get(0).getContext('2d')
    var objLineChart             = new Chart( objLineChartCanvas );
    var arrmixLineChartOptions   = arrLineChartOptions
    arrmixLineChartOptions.datasetFill = false
    objLineChart.Line( arrLineChartData, arrmixLineChartOptions );
    //objLineChart.destroy();
}

function createDonutChart( arrDonutChartData ) {
    
    var strDonutChart = $( '#js-donut-chart' ).get(0).getContext('2d')
    var objDonutChart       = new Chart( strDonutChart )
    
    var objDonutOptions     = {
     
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var objChart = objDonutChart.Doughnut( arrDonutChartData, objDonutOptions );
    document.getElementById( "js-donut-legend" ).innerHTML = objChart.generateLegend();
    
}

function createPieChart( arrPieChartData ) {
    
    var strPieChart = $( '#js-pie-chart' ).get(0).getContext('2d')
    var objPieChart       = new Chart( strPieChart )
    
    var objPieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 0, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeInQuart',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var objChart = objPieChart.Doughnut( arrPieChartData, objPieOptions )
    document.getElementById( "js-pie-legend" ).innerHTML = objChart.generateLegend();
}

function createBarChartUsingChartJs( arrmixChartData, objChartHtml, objOptions ) {
    
    if( objChart ) {
        objChart.destroy();
    }
    
    var objChart = new Chart( objChartHtml, {
                        type: 'bar',
                        data: {
                            labels: arrmixChartData['strXAxis'],
                            datasets: [{
                                label: arrmixChartData['strDatasetLabel'],
                                data: arrmixChartData['strYAxis'],
                                fill: false,
                                backgroundColor: 'rgb(31, 217, 7)',
                                borderColor: 'rgb(31, 217, 7)',
                                borderWidth: 1
                            }]
                        },
                        options: objOptions
                    });
                    
    
}

function createLineChartUsingChartJs( arrmixChartData, objChartHtml, objOptions ) {
    
    if( objChart ) {
        objChart.destroy();
    }
    
    var objChart = new Chart( objChartHtml, {
                        type: 'line',
                        data: {
                            labels: arrmixChartData['strXAxis'],
                            datasets: [{
                                label: arrmixChartData['strDatasetLabel'],
                                data: arrmixChartData['strYAxis'],
                                fill: false,
                                borderColor: 'rgb( 66, 158, 245 )',
                                borderWidth: 1
                            }]
                        },
                        options: objOptions
                    });
                    
    
}

function createDoughnutChartUsingChartJs( arrmixChartData, objChartHtml, objOptions ) {
    
    if( objChart ) {
        objChart.destroy();
    }
    
    var objChart = new Chart( objChartHtml, {
                                type: 'doughnut',
                                data: {
                                    labels: arrmixChartData['arrmixLables'],
                                    datasets: [{
                                        label: arrmixChartData['strDataSetLable'],
                                        data: arrmixChartData['arrmixValues'],
                                        backgroundColor: arrmixChartData['arrstrBackgroundColor']
                                    }]
                                },
                                options: objOptions

                            });
                    
    
}

function createPieChartUsingChartJs( arrmixChartData, objChartHtml, objOptions ) {
    
    if( objChart ) {
        objChart.destroy();
    }
    
    var objChart = new Chart( objChartHtml, {
                                type: 'pie',
                                data: {
                                    labels: arrmixChartData['arrmixLables'],
                                    datasets: [{
                                        label: arrmixChartData['strDataSetLable'],
                                        data: arrmixChartData['arrmixValues'],
                                        backgroundColor:[
                                           "rgb(255, 205, 86)",
                                           "rgb(54, 162, 235)"
                                        ]
                                    }]
                                },
                                options: objOptions

                            });
                    
    
}


