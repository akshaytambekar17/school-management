<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = ['common','school_management','array','form'];
        public $strBaseUrl;
        public $request;
        public $validation;
        public $session;
        public $email;
        public $objPasswordEncrypter;
        
        public static $s_arrmixValidationErrors;
        public static $s_objSession;

        /**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
                
                $this->strBaseUrl = getenv( 'app.baseURL' );
                $this->request = \Config\Services::request();
                $this->validation = \Config\Services::validation();
                $this->session = \Config\Services::session();
                $this->email =  \Config\Services::email();
                
                //self::setSession( $this->session );
                
                $objPasswordConfig = new \Config\Encryption();
                $objPasswordConfig->key = 'diligence_password@encrypter';
                
                $this->objPasswordEncrypter = \Config\Services::encrypter( $objPasswordConfig );
                
                $objConfig = new \Config\Encryption();
                $objConfig->key = 'diligence@encrypter';
                
                $this->objEncrypter = \Config\Services::encrypter( $objConfig );
	}
        
        protected function curlReq($url, $vars) {
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_POST, 1);
            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $vars);
            $buffer = curl_exec($curl_handle);
            curl_close($curl_handle);
            $object = json_decode($buffer);
            return $object;
        }

        protected function sendSms($mobileno, $textmessage) {
            //Your authentication key
            $authKey = "149798ARBQ5C3uSC958f9edd0";
            //Multiple mobiles numbers separated by comma
            $mobileNumber = $mobileno;
            //Sender ID,While using route4 sender id should be 6 characters long.
            $senderId = "ShiftMe";
            //Your message to send, Add URL encoding here.
            $message = urlencode($textmessage);
            //Define route
            $route = "1";
            //Prepare you post parameters
            $postData = array(
                'authkey' => $authKey,
                'mobiles' => $mobileNumber,
                'message' => $message,
                'sender' => $senderId,
                'route' => $route
            );
            //API URL
            $url = "https://control.msg91.com/api/sendhttp.php";
            // init the resource
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData
                    //,CURLOPT_FOLLOWLOCATION => true
            ));
            //Ignore SSL certificate verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            //get response
            $output = curl_exec($ch);
            //Print error if any
            if (curl_errno($ch)) {
                echo 'error:' . curl_error($ch);
                return FALSE;
            }
            curl_close($ch);
            return TRUE;
    //        echo $output;
        }

        protected function sendEmail( $strTo, $strSubject, $strMessage, $strAttachmentUrl = '' ) {
            
            $arrConfig = array();
            $arrConfig['protocol'] = 'smtp';
            $arrConfig['SMTPHost'] = 'mail.supremecluster.com';
            $arrConfig['SMTPPort'] = 465;
            $arrConfig['SMTPUser'] = SMTP_USERNAME;
            $arrConfig['SMTPPass'] = SMTP_PASSWORD;
            $arrConfig['SMTPCrypto'] = 'ssl';
            $arrConfig['mailType'] = 'html';
            $arrConfig['charset'] = 'utf-8';
            $arrConfig['wordWrap'] = true;
            $arrConfig['newline'] = "\r\n";
             
            $this->email->initialize( $arrConfig );
            $this->email->setTo( $strTo );
            $this->email->setFrom( SMTP_USERNAME, 'Diligenece System' );
            $this->email->setSubject( $strSubject );
            $this->email->setMessage( $strMessage );
            
            if( true == isVal( $strAttachmentUrl ) ) {
                $this->email->attach( $strAttachmentUrl );
            }

            if( $this->email->send() ) {
                $arrmixResponse['success'] = true;
                $arrmixResponse['message'] = "Email has been sent Successfully";
            } else {
                //printDie( $this->email->printDebugger() );
                $arrmixResponse['success'] = false;
                $arrmixResponse['message'] = "Something went wrong please try again";
            }
            
            return $arrmixResponse;
        }

        protected function response( $arrmixData ) {
            echo json_encode( $arrmixData );
        }
        
        protected function encryptPassword( $strPassword ) {
            return base64_encode( $this->objPasswordEncrypter->encrypt( $strPassword ) );
        }

        protected function decryptPassword( $strEncryptedPassword ) {
            return $this->objPasswordEncrypter->decrypt( base64_decode( $strEncryptedPassword ) );
        }
        
        protected function encryptToken( $arrmixTokenData ) {
            return base64_encode( $this->objEncrypter->encrypt( serialize( $arrmixTokenData ) ) );
        }

        protected function decryptToken( $strEncryptedToken ) {
            return ( array ) unserialize( $this->objEncrypter->decrypt( base64_decode( $strEncryptedToken ) ) );
        }
        
        public static function setFormValidationErrors( $arrmixValidationErrors ) {
            self::$s_arrmixValidationErrors = $arrmixValidationErrors;
        }
    
        public static function getFormValidationErrors() {
            return self::$s_arrmixValidationErrors;
        }

}
