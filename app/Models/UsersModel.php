<?php namespace App\Models;

use App\Models\CommonModel;

class UsersModel extends CommonModel
{
    protected $table      = 'tbl_users';
    protected $primaryKey = 'user_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'user_type', 'username', 'password', 'first_name', 'last_name', 'email_id', 'mobile_no', 'gender', 
        'address', 'city_id', 'state_id', 'is_verified', 'is_approved', 'created_by', 'updated_by'
    ];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
   
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
    
    public static function createService() {
        return new UsersModel();
    }
    
    public function createBuilderObject() {
        return self::createService()->builder();
    }
    
    public function findUserWithoutAdminList() {
        $objQuery = $this->createBuilderObject()->select( 'tbl_users.*, ts.state_name, tc.city_name' )
                                    ->join( 'tbl_states ts', 'ts.state_id = tbl_users.state_id', 'left' )
                                    ->join( 'tbl_cities tc', 'tc.city_id = tbl_users.city_id', 'left' )        
                                    ->where( 'user_id !=', ADMIN_ID )
                                    ->orderBy( 'user_id', 'desc' )
                                    ->get();
        return $objQuery->getResultArray();
    }
    
    public function findUserByUserId( $intUserId ) {
        
        $objQuery = $this->createBuilderObject()->select( 'tbl_users.*, ts.state_name, tc.city_name' )
                                    ->join( 'tbl_states ts', 'ts.state_id = tbl_users.state_id', 'left' )
                                    ->join( 'tbl_cities tc', 'tc.city_id = tbl_users.city_id', 'left' )        
                                    ->where( 'user_id', $intUserId )
                                    ->get();
        return $objQuery->getRowArray();
    }
    
    public function findUserByUsername( $strUsername ) {
        $objQuery = $this->createBuilderObject()->select( 'tbl_users.*, ts.state_name, tc.city_name, tstu.student_id, tstu.standard_id, tstu.roll_number' )
                                    ->join( 'tbl_students tstu', 'tbl_users.user_id = tstu.user_id' )
                                    ->join( 'tbl_states ts', 'ts.state_id = tbl_users.state_id', 'left' )
                                    ->join( 'tbl_cities tc', 'tc.city_id = tbl_users.city_id', 'left' )        
                                    ->where( 'tbl_users.username', $strUsername )
                                    ->get();
        return $objQuery->getRowArray();
    }
    
    public function add( $arrmixInsertData ) {
        
        $arrmixInsertData['created_by'] = $this->getCreatedBy();
        $arrmixInsertData['updated_by'] = $this->getUpdatedBy();
        $arrmixInsertData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->insert( $arrmixInsertData );
        if( $objQuery ) {
            return $objQuery->connID->insert_id;
        } 
        
        return false;
    }
    
    public function addBatch( $arrmixInsertList ) {
        
        $objQuery = $this->createBuilderObject()->insertBatch( $arrmixInsertList );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
    
    
    public function edit( $arrmixUpdateData ) {
        
        $arrmixUpdateData['updated_by'] = $this->getUpdatedBy();
        $arrmixUpdateData['updated_at'] = CURRENT_DATETIME;
        
        $objQuery = $this->createBuilderObject()->where( 'user_id', $arrmixUpdateData['user_id'] )
                                                ->update( $arrmixUpdateData );
        if( $objQuery ) {
            return true;
        } 
        
        return false;
    }
}