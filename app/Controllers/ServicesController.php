<?php
namespace App\Controllers;

use App\Controllers\BaseController;

class ServicesController extends BaseController {
    
    public static $s_intAppVersion;
    public static $s_strMethodName;
    public static $s_arrmixAuthDetails;
    public static $s_arrmixParameterDetails;
    public static $s_arrmixTokenDetails;
    
    public static $s_arrmixSkipMethod = ['login', 'fetchStandardList'];
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
	parent::initController($request, $response, $logger);
        
        $objRequestRawData = file_get_contents( "php://input" );
        if( false == isVal( $objRequestRawData ) ) {
            $this->generateErrorMessage( 'Raw data request is required.', ERROR_INVALID_REQUEST, ERROR_MESSAGE_INVALID_REQUEST );
        }
        
        $this->validateRequestData( $objRequestRawData );
        
    }   
    
    public function validateRequestData( $objRequestRawData ) {
        
        $arrmixRequestRawData = json_decode( $objRequestRawData, true );
        
        if( false == isset( $arrmixRequestRawData['app_version'] ) ) {
            $this->generateErrorMessage( 'App version is required.', ERROR_INVALID_REQUEST, ERROR_MESSAGE_INVALID_REQUEST );
        }
        
        if( false == isset( $arrmixRequestRawData['method'] ) && false == isArrVal( $arrmixRequestRawData['method'] ) ) {
            $this->generateErrorMessage( 'Method is required.', ERROR_INVALID_REQUEST, ERROR_MESSAGE_INVALID_REQUEST );
        }
        
        if( false == isset( $arrmixRequestRawData['method']['name'] ) && false == isStrVal( $arrmixRequestRawData['method']['name'] ) ) {
            $this->generateErrorMessage( 'Method name is required.', ERROR_INVALID_REQUEST, ERROR_MESSAGE_INVALID_REQUEST );
        }
        
        if( false == isset( $arrmixRequestRawData['method']['parameters'] ) ) {
            $this->generateErrorMessage( 'Parameters is required.', ERROR_INVALID_REQUEST, ERROR_MESSAGE_INVALID_REQUEST );
        }
        
        if( false == isset( $arrmixRequestRawData['auth'] ) ) {
            $this->generateErrorMessage( 'Auth type is required.', ERROR_INVALID_REQUEST, ERROR_MESSAGE_INVALID_REQUEST );
        }
        
        if( false == in_array( $arrmixRequestRawData['method']['name'], self::$s_arrmixSkipMethod ) ) {
            if( false == isset( $arrmixRequestRawData['auth']['token'] ) || false == isVal( $arrmixRequestRawData['auth']['token'] ) ) {
                $this->generateErrorMessage( 'Auth token is required.', ERROR_INVALID_REQUEST, ERROR_MESSAGE_INVALID_REQUEST );
            }
            $arrmixDecryptTokenData = $this->decryptToken( $arrmixRequestRawData['auth']['token'] );
            
            if( false == isset( $arrmixDecryptTokenData['user_id'] ) || false == isset( $arrmixDecryptTokenData['user_type_id'] ) || false == isset( $arrmixDecryptTokenData['login_at'] ) ||
               false == isIdVal( $arrmixDecryptTokenData['user_id'] ) || false == isIdVal( $arrmixDecryptTokenData['user_type_id'] ) ) {
                
                $this->generateErrorMessage( ERROR_MESSAGE_INVALID_TOKEN, ERROR_INVALID_TOKEN );
            }
            
            $arrmixUserDetails = \App\Models\UsersModel::createService()->where( ['user_id' => $arrmixDecryptTokenData['user_id'], 'user_type_id' => $arrmixDecryptTokenData['user_type_id'] ] )->findOne();
            if( false == isArrVal( $arrmixUserDetails ) ) {
                $this->generateErrorMessage( ERROR_MESSAGE_INVALID_TOKEN, ERROR_INVALID_TOKEN );
            }
            
            self::setDecryptedTokenDetails( $arrmixDecryptTokenData );
        }
        
        self::setRequestAuthDetails( $arrmixRequestRawData['auth'] );
        self::setRequestMethodName( $arrmixRequestRawData['method']['name'] );
        self::setRequestParameterDetails( $arrmixRequestRawData['method']['parameters'] );
        
    }
    
    public function generateErrorMessage( $strErrorMessage, $intErrorType, $strErrorTypeName = '' ) {
        
        $arrmixResponseData['response'] = [
            'error' => $intErrorType,
            'message' =>  ( ( true == isStrVal( $strErrorTypeName ) ) ? $strErrorTypeName . ': ' : '' ) . $strErrorMessage
        ];
        
        $this->response( $arrmixResponseData );
        exit;
    }
    
    public function generateSuccessMessage( $arrmixResponseDetails ) {
        
        $arrmixResponseData['response'] = [
            'status' => SUCCESS,
            'data' =>  $arrmixResponseDetails
        ];
        
        $this->response( $arrmixResponseData );
        exit;
    }
    
    public static function setDecryptedTokenDetails( $arrmixDecryptTokenData ) {
        self::$s_arrmixTokenDetails = $arrmixDecryptTokenData;
    }
    
    public static function getDecryptedTokenDetails() {
        return self::$s_arrmixTokenDetails;
    }
    
    public static function setRequestAuthDetails( $arrmixAuthDetails ) {
        self::$s_arrmixAuthDetails = $arrmixAuthDetails;
    }
    
    public static function getRequestAuthDetails() {
        return self::$s_arrmixAuthDetails;
    }
    
    public static function setRequestMethodName( $strMethodName ) {
        self::$s_strMethodName = $strMethodName;
    }
    
    public static function getRequestMethodName() {
        return self::$s_strMethodName;
    }
    
    public static function setRequestParameterDetails( $arrmixParameterDetails ) {
        self::$s_arrmixParameterDetails = $arrmixParameterDetails;
    }
    
    public static function getRequestParameterDetails() {
        return self::$s_arrmixParameterDetails;
    }
    
    public static function createService() {
        return new ServicesController();
    }
    
   
}
