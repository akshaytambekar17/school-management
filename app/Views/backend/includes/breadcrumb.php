<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= ( true == isset( $strContentHeader ) && true == isStrVal( $strContentHeader ) ) ? $strContentHeader : '' ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <?php
                    $boolIsBreadCrumbActive = false;
                    if( true == isset( $arrmixBreadCrumbList ) && true == isArrVal( $arrmixBreadCrumbList ) ) { 
                        $boolIsBreadCrumbActive = true;
                    }
                ?> 
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item <?= ( false == $boolIsBreadCrumbActive ) ? 'active' : '' ?>"><a href="<?= getBaseUrl() ?>admin"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</a></li>
                    <?php   
                        if( true == $boolIsBreadCrumbActive ) { 
                            foreach( $arrmixBreadCrumbList as $arrmixBreadCrumbDetails ) {
                                if( true == $arrmixBreadCrumbDetails['is_active'] ) {
                    ?>
                                    <li class="breadcrumb-item active"><?= $arrmixBreadCrumbDetails['name'] ?></li>
                    <?php       } else { ?>    
                                    <li class="breadcrumb-item"><a href="<?= $arrmixBreadCrumbDetails['href'] ?>"><?= $arrmixBreadCrumbDetails['name'] ?></a></li>   
                        
                    <?php } } }?>    
                    
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>