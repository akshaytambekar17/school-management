/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Akshay Tambekar
 * Created: Jul 10, 2020
 */

/**
 * 04-08-2020
 */

ALTER TABLE `tbl_subjects` ADD `standard_id` INT(11) NOT NULL AFTER `subject_id`;

ALTER TABLE `tbl_subjects` ADD CONSTRAINT `fkt_subject_standard_id` FOREIGN KEY (`standard_id`) REFERENCES `tbl_standards`(`standard_id`) ON DELETE CASCADE ON UPDATE NO ACTION;