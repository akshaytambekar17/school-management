/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const PRODUCT_TYPE_GATEWAY = 1;
const PRODUCT_TYPE_REPEATER = 2;
const PRODUCT_TYPE_ENDNODE_SENSOR = 3;

const PHYSICAL_LAYER_ETHERNET = 1;
const PHYSICAL_LAYER_WIFI = 2;
const PHYSICAL_LAYER_CELLULAR = 3;

const SERVER_SELECTION_DILIGENCE = 1;
const SERVER_SELECTION_USER = 2;
const SERVER_SELECTION_BOTH = 3;

const PROTOCOL_TYPE_TCP_MODBUS = 1;
const PROTOCOL_TYPE_MQTT = 2;
const PROTOCOL_TYPE_REST = 3;
const PROTOCOL_TYPE_TCP_IP = 4;

$( function () {
    $( '.js-validation-error' ).each( function() {
        $( this ).prevAll( '.form-control' ).addClass( 'is-invalid' );
        $( this ).prevAll().find( 'input[type=radio]' ).addClass( 'is-invalid' );
        $( this ).prevAll().find( 'input[type=checkbox]' ).addClass( 'is-invalid' );
        
    } );

    $( '.js-select2' ).select2();
    
    $( '.hide' ).hide();
    
    $( '.show' ).show();
    
    $( '.js-alert' ).delay( 7000 ).slideUp( 200, function() {
        $( this ).alert( 'close' );
    });

     $( '[data-toggle="tooltip"]' ).tooltip();
} );

function showAlertMessage( strAlert, strAlertMessage ) {
    
    var strIcon = '';
    if( 'success' == strAlert ) {
        strIcon = 'check';
    } else if( 'danger' == strAlert ) { 
        strIcon = 'ban';
    } else if( 'warning' == strAlert ) { 
        strIcon = 'exclamation-triangle';
    } else if( 'info' == strAlert ) { 
        strIcon = 'info';
    } 
    
    if( '' != strIcon ) {
        var strHtml = '<div class="alert alert-' + strAlert + ' alert-dismissible js-alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fas fa-' + strIcon + '"></i> ' + strAlertMessage + '</div>';
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        $( '.js-show-alert-message' ).html( '' );
        $( '.js-show-alert-message' ).html( strHtml );
        $( '.js-alert' ).fadeIn().delay( 7000 ).fadeOut( function () {
            $(this).remove();
        });
    }
}

function validateEmail( strEmail ) {
    var strEmailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if( strEmailFormat.test( strEmail ) ) {
        return true;
    }else{
        return false;
    }
}

function validateMobileNumber( intMobileNumber ) {
    var strNumberFormat = /^\d{10}$/;
    if( strNumberFormat.test( intMobileNumber ) ) {
        return true;
    }else{
        return false;
    }

}

function addValidationErrorInputField( strElement, strErrorMessage = '' ) {
    if( '' == strErrorMessage ) {
        strErrorMessage = 'The ' + $( strElement ).prev().text().replace( ":", "" ) + ' field is required.';
    }
    
    $( strElement ).after( '<span class="error invalid-feedback js-validation-error">' + strErrorMessage + '</span>' );
    $( strElement ).addClass( 'is-invalid' );
}

function addValidationErrorSelect2Field( strElement, strErrorMessage = '' ) {
    if( '' == strErrorMessage ) {
        strErrorMessage = 'The ' + $( strElement ).prev().text().replace( ":", "" ) + ' field is required.';
    }
    $( strElement ).next( '.select2-container' ).after( '<span class="error invalid-feedback js-validation-error">' + strErrorMessage +'</span>' );
    $( strElement ).addClass( 'is-invalid' );
}

function removeValidationErrors() {
    $( '.js-validation-error' ).remove();
    $( '.form-control' ).removeClass( 'is-invalid' );
}

function resetDeleteConfirmationModal() {
    $( "#js-modal-body-hidden-id" ).val(0);
    $( "#js-modal-body-header-name" ).html( '' );
}

function showDeleteConfirmationModal( strHeaderName, intHiddenId ) {
    $( "#js-modal-body-header-name" ).html( strHeaderName );
    $( "#js-modal-body-hidden-id" ).val( intHiddenId );
    $( "#js-modal-delete-confirmation" ).modal( 'show' );
}

function getCityListByStateId( intStateId, intHiddenCityId = '' ){
 
    $.ajax({
        type: "POST",
        url:  getBaseUrl() + 'fetch-city-list-by-state-id',
        data: { 'state_id' : intStateId, 'hidden_city_id' : intHiddenCityId },
        dataType: "html",
        success: function( objResult ){
            var strHtml = $.parseJSON( objResult );
            $( '#js-city-id' ).html('<option disabled selected> Select City</option>');
            $( '#js-city-id' ).append( strHtml );
        }
    });
}