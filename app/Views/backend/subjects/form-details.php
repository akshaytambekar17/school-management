<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><?= $strCardTitle; ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="<?= getBaseUrl() ?>admin/subjects/<?= true == isset( $arrmixSubjectDetails ) ? 'edit?subject_id=' . $arrmixSubjectDetails['subject_id'] : 'add' ?>"  method="post" name="form">
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label label-required" for="js-subject-name">Subject Name</label>
                        <input type="text" class="form-control" id="js-subject-name" name="subject_name" placeholder="Subject Name" value="<?= ( true == isset( $arrmixSubjectDetails ) ) ? $arrmixSubjectDetails['subject_name'] : set_value( 'subject_name' ) ?>">
                        <?= getValidationError( 'subject_name' ) ?>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label label-required" for="js-standard-id">Standard</label>
                        <select class="form-control js-select2" name="standard_id" id="js-standard-id">
                            <option selected="selected" disabled="disabled">Select Standard</option>
                            <?php 
                                foreach( $arrmixStandardList as $arrmixStandardDetails ) { 
                                    $strSelected = '';
                                    if( true == isset( $arrmixSubjectDetails ) && $arrmixSubjectDetails['standard_id'] == $arrmixStandardDetails['standard_id'] ) {
                                        $strSelected = 'selected="selected"';
                                    }
                            ?>
                                    <option value="<?= $arrmixStandardDetails['standard_id'] ?>" <?= set_select( 'standard_id', $arrmixStandardDetails['standard_id'] );  ?> <?= $strSelected?> ><?= $arrmixStandardDetails['standard_name'] ?></option>
                            <?php } ?>        
                        </select>
                        <?= getValidationError( 'standard_id' ) ?>
                    </div>
                </div>
                <?php if( true == isset( $arrmixSubjectDetails ) ) { ?>
                        <input type="hidden" name="subject_id" value="<?= $arrmixSubjectDetails['subject_id']?>">
                <?php } ?>    
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                    <a href="<?= getBaseUrl() ?>admin/subjects" class="btn btn-outline-danger">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>

