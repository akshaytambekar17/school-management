<?php
namespace App\Controllers\Services;

use App\Controllers\ServicesController;
use App\Models\UploadingVideosModel;

class UploadingVideosController extends ServicesController {
    
    public function actionfetchVideoList() { 
        $arrmixParameterDetails = self::getRequestParameterDetails();
        
        if( true == isset( $arrmixParameterDetails['standard_id'] ) && false == isIdVal( $arrmixParameterDetails['standard_id'] ) ) {
            $this->generateErrorMessage( 'Invalid Parameters', ERROR_INVALID_PARAMETERS, ERROR_MESSAGE_INVALID_PARAMETERS );
        } 
        
        if( true == isset( $arrmixParameterDetails['subject_id'] ) && false == isIdVal( $arrmixParameterDetails['subject_id'] ) ) {
            $this->generateErrorMessage( 'Invalid Parameters', ERROR_INVALID_PARAMETERS, ERROR_MESSAGE_INVALID_PARAMETERS );
        } 
        
        if( true == isset( $arrmixParameterDetails['standard_id'] ) && false == isset( $arrmixParameterDetails['subject_id'] ) ) {
            $arrmixUploadingVideoList = UploadingVideosModel::createService()->findUploadingVideoListByStandardId( $arrmixParameterDetails['standard_id'] );
            
        } else if( true == isset( $arrmixParameterDetails['subject_id'] ) && false == isset( $arrmixParameterDetails['standard_id'] ) ) {
            $arrmixUploadingVideoList = UploadingVideosModel::createService()->findUploadingVideoListBySubjectId( $arrmixParameterDetails['subject_id'] );
            
        } else {
            $arrmixUploadingVideoList = UploadingVideosModel::createService()->findUploadingVideoListByStandardIdBySubjectId( $arrmixParameterDetails['standard_id'], $arrmixParameterDetails['subject_id'] );
        }
        
        if( true == isArrVal( $arrmixUploadingVideoList  ) ) {
            
            foreach( $arrmixUploadingVideoList as $arrmixUploadingVideoDetails ) {
                $arrmixUploadingVideoDetails['videos'] = getBaseUrl() . 'uploaded-videos/' . $arrmixUploadingVideoDetails['videos'];
                $arrmixUploadingVideoData[] = $arrmixUploadingVideoDetails;
            }
            
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['video_list'] = $arrmixUploadingVideoData;
        } else {
            $arrmixResponseData['success'] = false;
            $arrmixResponseData['message'] = 'No videos found.';
        }
        
        $this->generateSuccessMessage( $arrmixResponseData );
    }
    
    public function actionfetchVideoDetails() { 
        $arrmixParameterDetails = self::getRequestParameterDetails();
        
        if( false == isset( $arrmixParameterDetails['uploading_video_id'] ) || false == isIdVal( $arrmixParameterDetails['uploading_video_id'] ) ) {
            $this->generateErrorMessage( 'Uploading Video Id is missing', ERROR_REQUEST_PARAMETER_MISSING, ERROR_MESSAGE_REQUEST_PARAMETER_MISSING );
        } 
        
        $arrmixUploadingVideoDetails = UploadingVideosModel::createService()->findUploadingVideoDetailsByUploadingVideoId( $arrmixParameterDetails['uploading_video_id'] );
        
        if( true == isArrVal( $arrmixUploadingVideoDetails  ) ) {
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['video_details'] = $arrmixUploadingVideoDetails;
        } else {
            $arrmixResponseData['success'] = false;
            $arrmixResponseData['message'] = 'No videos found.';
        }
        
        $this->generateSuccessMessage( $arrmixResponseData );
    }
    
    public static function createService() {
        return new UploadingVideosController();
    }
    
    
}
