<?php
namespace App\Controllers\Backend;

use App\Controllers\BackendController;
use App\Models\UploadingVideosModel;

class UploadingVideosController extends BackendController {
    
    public function actionList() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixUploadingVideoList = UploadingVideosModel::createService()->findUploadingVideoList();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Uploading Videos';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Uploading Videos', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Uploading Videos';
        $arrmixData['view'] = 'uploading-videos/list';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixUploadingVideoList'] = $arrmixUploadingVideoList;
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionAdd() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixStandardList = \App\Models\StandardsModel::createService()->findAll();
        $arrmixSubjectList = \App\Models\SubjectsModel::createService()->findAll();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Uploading Videos';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Uploading Videos', 'is_active' => false, 'href' => getBaseUrl() . 'admin/uploading-videos' ],
            ['name' => 'Add Video', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Add Video';
        $arrmixData['strCardTitle'] = 'Add Video';
        $arrmixData['view'] = 'uploading-videos/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixSubjectList'] = $arrmixSubjectList;
        $arrmixData['arrmixStandardList'] = $arrmixStandardList;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost();
            if( $this->validation->run( $arrmixRequestData , 'validateUploadingVideo' ) ) {
                $arrmixInsertData = $arrmixRequestData;
                
                $arrobjUploadVideo = $this->request->getFiles();
                if( true == isArrVal( $arrobjUploadVideo ) && true == isset( $arrobjUploadVideo['videos'] ) && true == isVal( $arrobjUploadVideo['videos']->getName() ) ) {
                    $strVideoErrorMessage = '';
                    
                    $arrmixAllowedMimeTypes = ['video/x-flv', 'video/mp4', 'video/3gpp', 'video/x-ms-wmv', 'video/x-msvideo','video/quicktime'];
                    if( false == in_array( $arrobjUploadVideo['videos']->getClientMimeType(), $arrmixAllowedMimeTypes ) ) {
                        $strVideoErrorMessage .= 'File which you upload is not allowed.';
                    }
                    
                    if( VIDEO_SIZE_LIMIT < $arrobjUploadVideo['videos']->getSize('mb') ) {
                        $strVideoErrorMessage .= ' Video size should not be greater than ' . VIDEO_SIZE . '.';
                    }
                    
                    $arrmixAllowedExtension = ['flv', 'mp4', '3gpp', 'wmv', 'msvideo','quicktime','mkv'];
                    if( false == in_array( $arrobjUploadVideo['videos']->guessExtension(), $arrmixAllowedExtension ) ) {
                        $strVideoErrorMessage .= ' File extension ' . $arrobjUploadVideo['videos']->guessExtension() . ' is not allowed.';
                    }
                    
                    if( false == isVal( $strVideoErrorMessage ) ) {
                        $arrmixInsertData['original_video_name'] = $arrobjUploadVideo['videos']->getName();
                        
                        $strTemporaryName = $arrobjUploadVideo['videos']->getRandomName();
                        $arrobjUploadVideo['videos']->move( './uploaded-videos', $strTemporaryName );
                        
                        $arrmixInsertData['videos'] = $strTemporaryName;
                        
                        
                        $intUploadingVideoId = UploadingVideosModel::createService()->add( $arrmixInsertData );

                        if( true == isIdVal( $intUploadingVideoId ) ) {
                            $this->session->setFlashdata( 'success', 'New video has been added successfully.' );
                            return redirect()->route( 'admin/uploading-videos' );
                        } else {
                            $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                        }
                    } else {
                        $this->session->setFlashdata( 'error', $strVideoErrorMessage );
                    }
                } else {
                    $this->session->setFlashdata( 'error', 'Please select the video.' );
                }
                
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionEdit() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getGet();
        
        if( false == isset( $arrmixRequestData['uploading_video_id'] ) || false == isIdVal( $arrmixRequestData['uploading_video_id'] ) ) {
            $this->session->setFlashdata( 'error', 'Invalid UploadingVideoId.' );
            return redirect()->route( 'admin/uploading-videos' );
        }
        
        $arrmixUploadingVideoDetails = UploadingVideosModel::createService()->findUploadingVideoDetailsByUploadingVideoId( $arrmixRequestData['uploading_video_id'] );
        
        if( false == isArrVal( $arrmixUploadingVideoDetails ) ) {
            $this->session->setFlashdata( 'error', 'Data not found for given UploadingVideoId : ' . $arrmixRequestData['uploading_video_id'] );
            return redirect()->route( 'admin/uploading-videos' );
        }
        
        $arrmixStandardList = \App\Models\StandardsModel::createService()->findAll();
        $arrmixSubjectList = \App\Models\SubjectsModel::createService()->findAll();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Upload Videos';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Upload Videos', 'is_active' => false, 'href' => getBaseUrl() . 'admin/uploading-videos' ],
            ['name' => $arrmixUploadingVideoDetails['standard_name'] . '-' . $arrmixUploadingVideoDetails['subject_name'], 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Edit Video Details';
        $arrmixData['strCardTitle'] = 'Edit Video Details';
        $arrmixData['view'] = 'uploading-videos/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixUploadingVideoDetails'] = $arrmixUploadingVideoDetails;
        $arrmixData['arrmixSubjectList'] = $arrmixSubjectList;
        $arrmixData['arrmixStandardList'] = $arrmixStandardList;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost(); 
            
            if( $this->validation->run( $arrmixRequestData , 'validateUploadingVideo' ) ) {                
                $arrmixUpdateData = $arrmixRequestData;
                $arrobjUploadVideo = $this->request->getFiles();
                
                $strVideoErrorMessage = '';
                if( true == isArrVal( $arrobjUploadVideo ) && true == isset( $arrobjUploadVideo['videos'] ) && true == isVal( $arrobjUploadVideo['videos']->getName() ) ) {
                    
                    $arrmixAllowedMimeTypes = ['video/x-flv', 'video/mp4', 'video/3gpp', 'video/x-ms-wmv', 'video/x-msvideo','video/quicktime'];
                    if( false == in_array( $arrobjUploadVideo['videos']->getClientMimeType(), $arrmixAllowedMimeTypes ) ) {
                        $strVideoErrorMessage .= 'File which you upload is not allowed.';
                    }
                    
                    if( VIDEO_SIZE_LIMIT < $arrobjUploadVideo['videos']->getSize('mb') ) {
                        $strVideoErrorMessage .= ' Video size should not be greater than ' . VIDEO_SIZE . '.';
                    }
                    
                    $arrmixAllowedExtension = ['flv', 'mp4', '3gpp', 'wmv', 'msvideo','quicktime','mkv'];
                    if( false == in_array( $arrobjUploadVideo['videos']->guessExtension(), $arrmixAllowedExtension ) ) {
                        $strVideoErrorMessage .= ' File extension ' . $arrobjUploadVideo['videos']->guessExtension() . ' is not allowed.';
                    }
                    
                    if( false == isVal( $strVideoErrorMessage ) ) {
                        $arrmixUpdateData['original_video_name'] = $arrobjUploadVideo['videos']->getName();
                        
                        $strTemporaryName = $arrobjUploadVideo['videos']->getRandomName();
                        $arrobjUploadVideo['videos']->move( './uploaded-videos', $strTemporaryName );
                        
                        $arrmixUpdateData['videos'] = $strTemporaryName;
                    } 
                }
                
                if( false == isVal( $strVideoErrorMessage ) ) {
                    $boolResult = UploadingVideosModel::createService()->edit( $arrmixUpdateData );
                
                    if( true ==$boolResult ) {
                        $this->session->setFlashdata( 'success', 'Video <b>' . $arrmixUploadingVideoDetails['standard_name'] . '-' . $arrmixUploadingVideoDetails['subject_name'] . '</b> has been updated successfully.' );
                        return redirect()->route( 'admin/uploading-videos' );
                    } else {
                        $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                    }
                } else {
                    $this->session->setFlashdata( 'error', $strVideoErrorMessage );
                }
                
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionDelete() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getPost();
        
        if( false == isset( $arrmixRequestData['uploading_video_id'] ) || false == isIdVal( $arrmixRequestData['uploading_video_id'] ) ) {
            $arrmixResponseData['success'] = false;
            $arrmixResponseData['message'] = 'Invalid UploadingVideoId';
            
            $this->response( $arrmixResponseData );
        }
        
        $arrmixResponseData['success'] = false;
        $arrmixResponseData['message'] = 'Something went wrong. Please try later';
        
        $objResponse = UploadingVideosModel::createService()->delete( $arrmixRequestData['uploading_video_id'] );
        
        if( $objResponse && true == $objResponse->connID->affected_rows ) {
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['message'] = 'Video has been successfully deleted.';
        } 
        
        $this->response( $arrmixResponseData );
    } 
    
}
