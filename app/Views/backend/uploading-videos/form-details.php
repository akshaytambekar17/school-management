<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><?= $strCardTitle; ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="<?= getBaseUrl() ?>admin/uploading-videos/<?= true == isset( $arrmixUploadingVideoDetails ) ? 'edit?uploading_video_id=' . $arrmixUploadingVideoDetails['uploading_video_id'] : 'add' ?>"  method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label label-required" for="js-standard-id">Standard</label>
                        <select class="form-control js-select2" name="standard_id" id="js-standard-id">
                            <option selected="selected" disabled="disabled">Select Standard</option>
                            <?php 
                                foreach( $arrmixStandardList as $arrmixStandardDetails ) { 
                                    $strSelected = '';
                                    if( true == isset( $arrmixUploadingVideoDetails ) && $arrmixUploadingVideoDetails['standard_id'] == $arrmixStandardDetails['standard_id'] ) {
                                        $strSelected = 'selected="selected"';
                                    }
                            ?>
                                    <option value="<?= $arrmixStandardDetails['standard_id'] ?>" <?= set_select( 'standard_id', $arrmixStandardDetails['standard_id'] );  ?> <?= $strSelected?> ><?= $arrmixStandardDetails['standard_name'] ?></option>
                            <?php } ?>        
                        </select>
                        <?= getValidationError( 'standard_id' ) ?>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label label-required" for="js-subject-id">Subject</label>
                        <select class="form-control js-select2" name="subject_id" id="js-subject-id">
                            <option selected="selected" disabled="disabled">Select Subject</option>
                            <?php 
                                foreach( $arrmixSubjectList as $arrmixSubjectDetails ) { 
                                    $strSelected = '';
                                    if( true == isset( $arrmixUploadingVideoDetails ) && $arrmixUploadingVideoDetails['subject_id'] == $arrmixSubjectDetails['subject_id'] ) {
                                        $strSelected = 'selected="selected"';
                                    }
                            ?>
                                    <option value="<?= $arrmixSubjectDetails['subject_id'] ?>" <?= set_select( 'subject_id', $arrmixSubjectDetails['subject_id'] );  ?> <?= $strSelected?> ><?= $arrmixSubjectDetails['subject_name'] ?></option>
                            <?php } ?>        
                        </select>
                        <?= getValidationError( 'subject_id' ) ?>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label label-required" for="js-topic">Topic</label>
                        <input type="text" class="form-control" id="js-topic" name="topic" placeholder="Topic" value="<?= ( true == isset( $arrmixUploadingVideoDetails ) ) ? $arrmixUploadingVideoDetails['topic'] : set_value( 'topic' ) ?>">
                        <?= getValidationError( 'topic' ) ?>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label label-required" for="js-upload-video">Upload Video</label>
                        <input type="file" name="videos" class="form-control" id="js-upload-video">
                        <?php if( true == isset( $arrmixUploadingVideoDetails ) ) { ?> 
                            <br>
                            Download: &nbsp;
                            <a href="<?= getBaseUrl() ?>uploaded-videos/<?= $arrmixUploadingVideoDetails['videos']?>" download><?= $arrmixUploadingVideoDetails['original_video_name']?></a>
                        <?php } ?> 
                        <?= getValidationError( 'videos' ) ?>
                    </div>
                </div>
                
                <?php if( true == isset( $arrmixUploadingVideoDetails ) ) { ?>
                        <input type="hidden" name="uploading_video_id" value="<?= $arrmixUploadingVideoDetails['uploading_video_id']?>">
                <?php } ?>    
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                    <a href="<?= getBaseUrl() ?>admin/uploading-videos" class="btn btn-outline-danger">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>

