<?php
namespace App\Controllers\Backend;

use App\Controllers\BackendController;
use App\Models\StudentsModel;

class StudentsController extends BackendController {
    
    public function actionList() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixStudentList = StudentsModel::createService()->findStudentList();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Students';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Students', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Students';
        $arrmixData['view'] = 'students/list';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixStudentList'] = $arrmixStudentList;
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionAdd() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixStateList = \App\Models\StatesModel::createService()->findAll();
        $arrmixStandardList = \App\Models\StandardsModel::createService()->findAll();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Students';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Students', 'is_active' => false, 'href' => getBaseUrl() . 'admin/students' ],
            ['name' => 'Add Student', 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Add Student';
        $arrmixData['strCardTitle'] = 'Add Student';
        $arrmixData['view'] = 'students/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixStateList'] = $arrmixStateList;
        $arrmixData['arrmixStandardList'] = $arrmixStandardList;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost();
            
            if( $this->validation->run( $arrmixRequestData , 'validateStudent' ) ) {
                $arrmixUserInsertData = $arrmixRequestData;
                
                unset( $arrmixUserInsertData['standard_id'] );
                unset( $arrmixUserInsertData['roll_number'] );
                unset( $arrmixUserInsertData['confirm_password'] );
                $arrmixUserInsertData['is_verified'] = VERIFIED;
                $arrmixUserInsertData['is_approved'] = APPROVED;
                $arrmixUserInsertData['user_type_id'] = USER_TYPE_STUDENT;
                $arrmixUserInsertData['password'] = $this->encryptPassword( $arrmixUserInsertData['password'] );
                
                $intUserId = \App\Models\UsersModel::createService()->add( $arrmixUserInsertData );
                if( true == isIdVal( $intUserId ) ) {
                    $arrmixStudentInsertData = [
                        'user_id'     => $intUserId,
                        'standard_id' => $arrmixRequestData['standard_id'],
                        'roll_number' => $arrmixRequestData['roll_number']
                    ];
                    
                    $intStudentId = StudentsModel::createService()->add( $arrmixStudentInsertData );
                
                    if( true == isIdVal( $intStudentId ) ) {
                        $this->session->setFlashdata( 'success', 'New student <b>' . $arrmixRequestData['first_name'] . ' ' . $arrmixRequestData['last_name'] . '</b> has been added successfully.' );
                        return redirect()->route( 'admin/students' );
                    } else {
                        $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                    }
                } else {
                    $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                }
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionEdit() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getGet();
        
        if( false == isset( $arrmixRequestData['student_id'] ) || false == isIdVal( $arrmixRequestData['student_id'] ) ) {
            $this->session->setFlashdata( 'error', 'Invalid StudentId.' );
            return redirect()->route( 'admin/students' );
        }
        
        $arrmixStudentDetails = StudentsModel::createService()->findStudentDetailsByStudentId( $arrmixRequestData['student_id'] );
        
        if( false == isArrVal( $arrmixStudentDetails ) ) {
            $this->session->setFlashdata( 'error', 'Data not found for given StudentId : ' . $arrmixRequestData['student_id'] );
            return redirect()->route( 'admin/students' );
        }
        
        $arrmixStateList = \App\Models\StatesModel::createService()->findAll();
        $arrmixStandardList = \App\Models\StandardsModel::createService()->findAll();
        
        $arrmixBreadCrumbData['strContentHeader'] = 'Students';
        $arrmixBreadCrumbData['arrmixBreadCrumbList'] = [
            ['name' => 'Students', 'is_active' => false, 'href' => getBaseUrl() . 'admin/students' ],
            ['name' => $arrmixStudentDetails['first_name'] . ' ' . $arrmixStudentDetails['last_name'], 'is_active' => true]
        ];
        
        $arrmixData = [];
        $arrmixData['strTitle'] = 'Edit Student';
        $arrmixData['strCardTitle'] = 'Edit Student';
        $arrmixData['view'] = 'students/form-details';
        $arrmixData['arrmixBreadCrumbData'] = $arrmixBreadCrumbData;
        $arrmixData['arrmixStudentDetails'] = $arrmixStudentDetails;
        $arrmixData['arrmixStateList'] = $arrmixStateList;
        $arrmixData['arrmixStandardList'] = $arrmixStandardList;
        
        if( $this->request->getPost() ) {
            $arrmixRequestData = $this->request->getPost(); 
            
            if( $this->validation->run( $arrmixRequestData , 'validateUpdateStudent' ) ) {
                
                $arrmixUserUpdateData = $arrmixRequestData;
                
                unset( $arrmixUserUpdateData['student_id'] );
                unset( $arrmixUserUpdateData['standard_id'] );
                unset( $arrmixUserUpdateData['roll_number'] );
                unset( $arrmixUserUpdateData['confirm_password'] );
                unset( $arrmixUserUpdateData['username'] );
                if( true == isVal( $arrmixUserUpdateData['password'] ) ) {
                    $arrmixUserUpdateData['password'] = $this->encryptPassword( $arrmixUserUpdateData['password'] );
                } else {
                    unset( $arrmixUserUpdateData['password'] );
                }
                
                $boolResult = \App\Models\UsersModel::createService()->edit( $arrmixUserUpdateData );
                if( true == $boolResult ) { 
                    
                    $arrmixStudentUpdateData = [
                        'user_id'     => $arrmixRequestData['user_id'],
                        'student_id'  => $arrmixRequestData['student_id'],
                        'standard_id' => $arrmixRequestData['standard_id'],
                        'roll_number' => $arrmixRequestData['roll_number']
                    ];
                    
                    $boolResult = StudentsModel::createService()->edit( $arrmixStudentUpdateData );

                    if( true == $boolResult ) {
                        $this->session->setFlashdata( 'success', 'Student ' . $arrmixRequestData['first_name'] . ' ' . $arrmixRequestData['last_name'] . ' has been updated successfully.' );
                        return redirect()->route( 'admin/students' );
                    } else {
                        $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                    }
                } else {
                    $this->session->setFlashdata( 'error', 'Something went wrong. Please try later.' );
                }    
                
            } else {
                $this->session->setFlashdata( 'error', 'Validation Errors. Please check the below errors.' );
                self::setFormValidationErrors( $this->validation->getErrors() );
            }
        }
        
        return $this->backendLayout( $arrmixData );
    }
    
    public function actionDelete() {
        
        if( false == isArrVal( $this->arrmixAdminSessionDetails ) ) {
            return redirect()->route( 'admin/authentication/login' );
        } 
        
        $arrmixRequestData = $this->request->getPost();
        
        if( false == isset( $arrmixRequestData['student_id'] ) || false == isIdVal( $arrmixRequestData['student_id'] ) ) {
            $arrmixResponseData['success'] = false;
            $arrmixResponseData['message'] = 'Invalid StudentId';
            
            $this->response( $arrmixResponseData );
        }
        
        $arrmixResponseData['success'] = false;
        $arrmixResponseData['message'] = 'Something went wrong. Please try later';
        
        $objResponse = StudentsModel::createService()->delete( $arrmixRequestData['student_id'] );
        
        if( $objResponse && true == $objResponse->connID->affected_rows ) {
            $arrmixResponseData['success'] = true;
            $arrmixResponseData['message'] = 'Successfully Deleted';
        } 
        
        $this->response( $arrmixResponseData );
    } 
    
}
