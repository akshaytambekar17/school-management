<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><?= $strCardTitle; ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="<?= getBaseUrl() ?>admin/students/<?= true == isset( $arrmixStudentDetails ) ? 'edit?student_id=' . $arrmixStudentDetails['student_id'] : 'add' ?>"  method="post" name="form">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-first-name">First Name</label>
                            <input type="text" class="form-control" id="js-first-name" name="first_name" placeholder="First Name" value="<?= ( true == isset( $arrmixStudentDetails ) ) ? $arrmixStudentDetails['first_name'] : set_value( 'first_name' ) ?>">
                            <?= getValidationError( 'first_name' ) ?>
                        </div>
                        
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-last-name">Last Name</label>
                            <input type="text" class="form-control" id="js-last-name" name="last_name" placeholder="Last Name" value="<?= ( true == isset( $arrmixStudentDetails ) ) ? $arrmixStudentDetails['last_name'] : set_value( 'last_name' ) ?>">
                            <?= getValidationError( 'last_name' ) ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4 form-group">
                            <label class="control-label" for="js-email-id">Email Id</label>
                            <input type="email" class="form-control" id="js-email-id" name="email_id" placeholder="Email Id" value="<?= ( true == isset( $arrmixStudentDetails ) ) ? $arrmixStudentDetails['email_id'] : set_value( 'email_id' ) ?>">
                            <?= getValidationError( 'email_id' ) ?>
                        </div>
                        
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-mobile-no">Mobile Number</label>
                            <input type="text" class="form-control" id="js-mobile-no" name="mobile_no" placeholder="Mobile Number" value="<?= ( true == isset( $arrmixStudentDetails ) ) ? $arrmixStudentDetails['mobile_no'] : set_value( 'mobile_no' ) ?>">
                            <?= getValidationError( 'mobile_no' ) ?>
                        </div>
                        
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-mobile-no">Gender</label><br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="js-radio-button-male" name="gender" class="custom-control-input" value="<?= GENDER_MALE ?>" <?= ( true == isset( $arrmixStudentDetails ) && GENDER_MALE == $arrmixStudentDetails['gender'] ) ? 'checked' : '' ?> >
                                <label class="custom-control-label" for="js-radio-button-male">Male</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="js-radio-button-female" name="gender" class="custom-control-input" value="<?= GENDER_FEMALE ?>" <?= ( true == isset( $arrmixStudentDetails ) && GENDER_FEMALE == $arrmixStudentDetails['gender'] ) ? 'checked' : '' ?> >
                                <label class="custom-control-label" for="js-radio-button-female">Female</label>
                            </div>
                            <?= getValidationError( 'gender' ) ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-standard-id">Standard</label>
                            <select class="form-control js-select2" name="standard_id" id="js-standard-id">
                                <option selected="selected" disabled="disabled">Select Standard</option>
                                <?php 
                                    foreach( $arrmixStandardList as $arrmixStandardDetails ) { 
                                        $strSelected = '';
                                        if( true == isset( $arrmixStudentDetails ) && $arrmixStudentDetails['standard_id'] == $arrmixStandardDetails['standard_id'] ) {
                                            $strSelected = 'selected="selected"';
                                        }
                                ?>
                                        <option value="<?= $arrmixStandardDetails['standard_id'] ?>" <?= set_select( 'standard_id', $arrmixStandardDetails['standard_id'] );  ?> <?= $strSelected?> ><?= $arrmixStandardDetails['standard_name'] ?></option>
                                <?php } ?>        
                            </select>
                            <?= getValidationError( 'standard_id' ) ?>
                        </div>
                        
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-roll-number">Roll Number</label>
                            <input type="text" class="form-control" id="js-roll-number" name="roll_number" placeholder="Roll Number" value="<?= ( true == isset( $arrmixStudentDetails ) ) ? $arrmixStudentDetails['roll_number'] : set_value( 'roll_number' ) ?>">
                            <?= getValidationError( 'roll_number' ) ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12 form-group">
                            <label class="control-label label-required" for="js-address">Address</label>
                            <input type="text" class="form-control" id="js-address" name="address" placeholder="Address" value="<?= ( true == isset( $arrmixStudentDetails ) ) ? $arrmixStudentDetails['address'] : set_value( 'address' ) ?>">
                            <?= getValidationError( 'address' ) ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-state-id">State</label>
                            <select class="form-control js-select2" name="state_id" id="js-state-id">
                                <option selected="selected" disabled="disabled">Select State</option>
                                <?php 
                                    foreach( $arrmixStateList as $arrmixStateDetails ) { 
                                        $strSelected = '';
                                        if( true == isset( $arrmixStudentDetails ) && $arrmixStudentDetails['state_id'] == $arrmixStateDetails['state_id'] ) {
                                            $strSelected = 'selected="selected"';
                                        }
                                ?>
                                        <option value="<?= $arrmixStateDetails['state_id'] ?>" <?= set_select( 'state_id', $arrmixStateDetails['state_id'] );  ?> <?= $strSelected?> ><?= $arrmixStateDetails['state_name'] ?></option>
                                <?php } ?>        
                            </select>
                            <?= getValidationError( 'state_id' ) ?>
                        </div>
                        
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-city-id">City</label>
                            <select class="form-control js-select2" name="city_id" id="js-city-id">
                                <option selected="selected" disabled="disabled">Select City</option>
                            </select>
                            <?= getValidationError( 'city_id' ) ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4 form-group">
                            <label class="control-label" for="js-username">Username</label>
                            <input type="text" class="form-control" id="js-username" name="username" placeholder="Username" value="<?= ( true == isset( $arrmixStudentDetails ) ) ? $arrmixStudentDetails['username'] : set_value( 'username' ) ?>" <?= ( true == isset( $arrmixStudentDetails ) ) ? 'disabled="disabled"' : '' ?> >
                            <?= getValidationError( 'username' ) ?>
                        </div>
                        
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-password">Password</label>
                            <input type="password" class="form-control" id="js-password" name="password" placeholder="Password" value="<?= set_value( 'password' ) ?>">
                            <?= getValidationError( 'password' ) ?>
                        </div>
                        
                        <div class="col-4 form-group">
                            <label class="control-label label-required" for="js-confirm-password">Confirm Password</label>
                            <input type="password" class="form-control" id="js-confirm-password" name="confirm_password" placeholder="Password" value="<?= set_value( 'confirm_password' ) ?>">
                            <?= getValidationError( 'confirm_password' ) ?>
                        </div>
                        
                    </div>
                </div>
                <?php if( true == isset( $arrmixStudentDetails ) ) { ?>
                        <input type="hidden" name="student_id" value="<?= $arrmixStudentDetails['student_id']?>">
                        <input type="hidden" name="user_id" value="<?= $arrmixStudentDetails['user_id']?>">
                        <input type="hidden" value="<?= $arrmixStudentDetails['city_id']?>" id="js-hidden-city-id">
                <?php } ?>    
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                    <a href="<?= getBaseUrl() ?>admin/students" class="btn btn-outline-danger"> Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>

<script>
    $(document).ready(function () {
        var intStateId = $( "#js-state-id" ).val();
        if( '' != intStateId && null != intStateId && 'undefined' != intStateId ) {
            var intHiddenCityId = $( '#js-hidden-city-id' ).val();
            getCityListByStateId( intStateId, intHiddenCityId );
        }
        $( "#js-state-id" ).on( 'change', function() {
            var intStateId = $( "#js-state-id" ).val();
            getCityListByStateId( intStateId );
        });
    });
</script>