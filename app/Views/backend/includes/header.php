<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= ( true == isset( $strTitle ) && true == isVal( $strTitle ) ) ? $strTitle : 'Wireless Sensing Networking' ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/daterangepicker/daterangepicker.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
 
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
        
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/daterangepicker/daterangepicker.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/select2/css/select2.min.css">
        
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        
        <link rel="stylesheet" href="<?= getBaseUrl();?>assets/backend/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/dist/css/adminlte.min.css">
        
        <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/backend/css/stylesheet.css">
    </head>
    <?php if( true == isset( $boolIsLogin ) && true == $boolIsLogin ) { ?>
        <body class="hold-transition login-page">
    <?php } else { ?>        
        <body class="hold-transition sidebar-mini layout-fixed">
    <?php } ?>        

 