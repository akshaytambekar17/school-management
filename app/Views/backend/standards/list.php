<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <a href="<?= getBaseUrl() ?>admin/standards/add" class="btn btn-outline-success"> <i class="fas fa-plus-square"></i>&nbsp;&nbsp;Add Standard</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped js-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Standard Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if( true == isArrVal( $arrmixStandardList ) ) {
                                foreach( $arrmixStandardList as $arrmixStandardDetails ) {
                        ?>
                                <tr>
                                    <td><?= $arrmixStandardDetails['standard_id']?></td>
                                    <td><?= $arrmixStandardDetails['standard_name']?></td>
                                    <td class="project-actions">
                                        <a class="btn btn-outline-info btn-sm" href="<?= getBaseUrl() ?>admin/standards/edit?standard_id=<?= $arrmixStandardDetails['standard_id']?>">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm" href="javascript:void(0)" id="js-delete-standard" data-standard_id="<?= $arrmixStandardDetails['standard_id']?>" data-standard_name="<?= $arrmixStandardDetails['standard_name']?>" onclick="showDeleteModal(this)">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                        <?php } } else { ?>    
                                <tr>
                                    <td colspan="7" class="text-center">No Standards found</td>
                                </tr>
                        <?php } ?>    
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        
        $( '#js-modal-body-confirm-button' ).on( 'click', function() {
            var intStandardId = $( "#js-modal-body-hidden-id" ).val();
            
            $.ajax({
                type: "POST",
                url: "<?php echo getBaseUrl(); ?>" + "admin/standards/delete",
                dataType: 'json',
                data: { 'standard_id' : intStandardId },
                success: function( arrmixResponseData ) {
                    $( '#js-modal-delete-confirmation' ).modal( 'hide' );
                    
                    resetDeleteConfirmationModal();
                    if( true == arrmixResponseData.success ) {
                        var strAlertMessage = 'Standard ' + $( '#js-modal-body-header-name' ).text() + ' has been deleted successfully';
                        showAlertMessage( 'success', strAlertMessage )
                        setTimeout(function(){ 
                            location.reload();
                        }, 2000);
                    }else{
                        showAlertMessage( 'danger', 'Something went wrong. Please try later' );
                    }
                }
            });
        });
    });
    
    function showDeleteModal(ths){
        var intStandardId = $( ths ).data( 'standard_id' );
        var strHeaderName = 'Standard <b>' + $( ths ).data( 'standard_name' ) + '</b>';
        showDeleteConfirmationModal( strHeaderName, intStandardId );
    }
</script>
