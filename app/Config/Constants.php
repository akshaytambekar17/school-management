<?php
date_default_timezone_set('Asia/Kolkata');
//--------------------------------------------------------------------
// App Namespace
//--------------------------------------------------------------------
// This defines the default Namespace that is used throughout
// CodeIgniter to refer to the Application directory. Change
// this constant to change the namespace that all application
// classes should use.
//
// NOTE: changing this will require manually modifying the
// existing namespaces of App\* namespaced-classes.
//
defined('APP_NAMESPACE') || define('APP_NAMESPACE', 'App');

/*
|--------------------------------------------------------------------------
| Composer Path
|--------------------------------------------------------------------------
|
| The path that Composer's autoload file is expected to live. By default,
| the vendor folder is in the Root directory, but you can customize that here.
*/
defined('COMPOSER_PATH') || define('COMPOSER_PATH', ROOTPATH . 'vendor/autoload.php');

/*
|--------------------------------------------------------------------------
| Timing Constants
|--------------------------------------------------------------------------
|
| Provide simple ways to work with the myriad of PHP functions that
| require information to be in seconds.
*/
defined('SECOND') || define('SECOND', 1);
defined('MINUTE') || define('MINUTE', 60);
defined('HOUR')   || define('HOUR', 3600);
defined('DAY')    || define('DAY', 86400);
defined('WEEK')   || define('WEEK', 604800);
defined('MONTH')  || define('MONTH', 2592000);
defined('YEAR')   || define('YEAR', 31536000);
defined('DECADE') || define('DECADE', 315360000);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        || define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          || define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         || define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   || define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  || define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') || define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     || define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       || define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      || define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      || define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define( 'SMTP_USERNAME', 'akshay.tambekar@soyomonitoring.com' );
define( 'SMTP_PASSWORD', 'akshay1793' );

define( 'ADMIN_MAIL_ID', 'akshaytambekar17@gmail.com' );

define( 'CURRENT_DATETIME', date('Y-m-d H:i:s') );
define( 'CURRENT_DATE', date('Y-m-d') );
define( 'CURRENT_TIME', date('H:i:s') );

define( 'ADMIN_ID', 1 );

define( 'ADMIN_USERNAME', 'adminmaster' );
define( 'SUPER_ADMIN_USERNAME', 'webmaster' );

define( 'INACTIVE', 1 );
define( 'ACTIVE', 2 );

define( 'APPROVED', 1 );
define( 'NOT_APPROVED', 0 );

define( 'VERIFIED', 1 );
define( 'NOT_VERIFIED', 0 );

define( 'GENDER_MALE', 1 );
define( 'GENDER_FEMALE', 2 );

define( 'PROJECT_NAME', 'School Management' );

define( 'EXPIRE_TIME', 60 );

define( 'VIDEO_SIZE_LIMIT', 10000000 );
define( 'VIDEO_SIZE', '10 MB' );

define( 'USER_TYPE_STUDENT', 2 );
define( 'USER_TYPE_ADMIN', 1 );


/**
 * Errors  
 **/

define( 'SUCCESS', 200 );

define( 'ERROR_INVALID_REQUEST', 301 );
define( 'ERROR_INVALID_TOKEN', 302 );
define( 'ERROR_REQUEST_PARAMETER_MISSING', 303 );
define( 'ERROR_METHOD_NAME_NOT_FOUND', 304 );
define( 'ERROR_INVALID_PARAMETERS', 305 );
define( 'ERROR_OTHER', 310 );

define( 'ERROR_MESSAGE_INVALID_REQUEST', 'Invalid Request' );
define( 'ERROR_MESSAGE_INVALID_PARAMETERS', 'Invalid Parameters' );
define( 'ERROR_MESSAGE_REQUEST_PARAMETER_MISSING', 'Request Parameter Missing' );
define( 'ERROR_MESSAGE_INVALID_TOKEN', 'Invalid Token' );
define( 'ERROR_MESSAGE_METHOD_NAME_NOT_FOUND', 'Invalid Method Name' );





