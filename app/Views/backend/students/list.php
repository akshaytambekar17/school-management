<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<script src="<?= getBaseUrl() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
<!-- Content Header (Page header) -->
<?php App\Controllers\BackendController::createService()->showBreadCrumbs( $arrmixBreadCrumbData ) ?>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="js-show-alert-message">
            <?php App\Controllers\BackendController::createService()->showAlertMessage( $arrmixFlashData ) ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <a href="<?= getBaseUrl() ?>admin/students/add" class="btn btn-outline-success"> <i class="fas fa-plus-square"></i>&nbsp;&nbsp;Add Student</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped js-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Student Name</th>
                            <th>Mobile Number</th>
                            <th>Gender</th>
                            <th>Standard</th>
                            <th>Roll Number</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if( true == isArrVal( $arrmixStudentList ) ) {
                                foreach( $arrmixStudentList as $arrmixStudentDetails ) {
                        ?>
                                <tr>
                                    <td><?= $arrmixStudentDetails['student_id']?></td>
                                    <td><?= $arrmixStudentDetails['first_name'] . ' ' . $arrmixStudentDetails['last_name']; ?></td>
                                    <td><?= $arrmixStudentDetails['mobile_no']?></td>
                                    <td>
                                        <?php 
                                            if( GENDER_FEMALE == $arrmixStudentDetails['gender'] ) {
                                                echo 'Female';
                                            } else {
                                                echo 'Male';
                                            } 
                                        ?>
                                    </td>
                                    <td><?= $arrmixStudentDetails['standard_name']?></td>
                                    <td><?= $arrmixStudentDetails['roll_number']?></td>
                                    <td class="project-actions">
                                        <a class="btn btn-outline-info btn-sm" href="<?= getBaseUrl() ?>admin/students/edit?student_id=<?= $arrmixStudentDetails['student_id']?>">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm" href="javascript:void(0)" id="js-delete-student" data-student_id="<?= $arrmixStudentDetails['student_id']?>" data-student_name="<?= $arrmixStudentDetails['first_name'] . ' ' . $arrmixStudentDetails['last_name']; ?>" onclick="showDeleteModal(this)">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                        <?php } } else { ?>    
                                <tr>
                                    <td colspan="7" class="text-center">No students record found</td>
                                </tr>
                        <?php } ?>    
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        
        $( '#js-modal-body-confirm-button' ).on( 'click', function() {
            var intStudentId = $( "#js-modal-body-hidden-id" ).val();
            
            $.ajax({
                type: "POST",
                url: "<?php echo getBaseUrl(); ?>" + "admin/students/delete",
                dataType: 'json',
                data: { 'student_id' : intStudentId },
                success: function( arrmixResponseData ) {
                    $( '#js-modal-delete-confirmation' ).modal( 'hide' );
                    
                    resetDeleteConfirmationModal();
                    if( true == arrmixResponseData.success ) {
                        var strAlertMessage = 'Student ' + $( '#js-modal-body-header-name' ).text() + ' has been deleted successfully';
                        showAlertMessage( 'success', strAlertMessage )
                        setTimeout(function(){ 
                            location.reload();
                        }, 2000);
                    }else{
                        showAlertMessage( 'danger', 'Something went wrong. Please try later' );
                    }
                }
            });
        });
    });
    
    function showDeleteModal(ths){
        var intStudentId = $( ths ).data( 'student_id' );
        var strHeaderName = 'Student <b>' + $( ths ).data( 'student_name' ) + '</b>';
        showDeleteConfirmationModal( strHeaderName, intStudentId );
    }
</script>
